This code depend on `ROOT`, `Tensorflow`, and `quickstats`.
It run under `LCG 102` with some customised python packages or an anaconda environment.

```
setupATLAS
lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
pip install --user quickstats
```
