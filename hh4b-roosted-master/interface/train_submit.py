import sys
framework = '/afs/cern.ch/user/z/zhangr/work/HH4b/hh4b-roosted'
sys.path.append(framework)

from cores.analysis import *

ana = analysis(framework=framework)
cache_path = '/eos/atlas/unpledged/group-wisc/users/zhangr/HH4b/NNT/SEP22-0/tmp/'
bkgmodel_df = ana.get_bkgmodel_df(cache_path)

print('iseed = ', sys.argv[1])
ana.train_study(precut=['noVBFcut', ], batch=True, nseeds=10, iseed=int(sys.argv[1]))

