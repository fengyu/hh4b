from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from tensorflow.keras.models import Model
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Input, BatchNormalization
from tensorflow.keras.models import load_model
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
import os
from os import path, environ, makedirs, stat
import pandas as pd
import json, pickle
import matplotlib.pyplot as plt
import numpy as np
from .plot_utils import yields_table, plot_1D, plot_1D_with_errorbar
from pdb import set_trace
from quickstats.utils.data_conversion import dataframe2root
from quickstats.plots.color_schemes import QUICKSTATS_PALETTES
color = QUICKSTATS_PALETTES["default"]

class bkgReweighter:
    def __init__(self, config):
        self.config = config
        self.rw_strategy=config.get('rw_strategy')
        self.train_which_precut = config.get('train_which_precut', 'SRtest1noVBFcut')
        self.apply_which_precut = config.get('apply_which_precut', self.train_which_precut)
        self.train_columns = config.get('train_columns', ['log_pt_h1Boosted', 'log_pt_h2Boosted', 'eta_h1Boosted', 'eta_h2Boosted', 'delta_ptHH_boosted', 'delta_etaHH_boosted'])
        self.nseeds = config.get('nseeds', 1)
        self.iseed = config.get('iseed', None)
        self.base_seed = config.get('base_seed', 1000)
        self.bootstrap = config.get('bootstrap', True)
        self.bootstrap_reserve_norm = config.get('bootstrap_reserve_norm', False)
        self.epochs = config.get('epochs', 100)
        self.patience = config.get('patience', 20)
        self.cache = config.get('cache', False)
        self.train_val_split = config.get('train_ratio', 0.7)
        self.bkgmodel_df = config['data_map']
        self.train_target = config.get('train_target', 'SR2F')
        self.train_origin = config.get('train_origin', 'CR2F')
        self.train_target = f'{self.train_target}data'
        self.train_origin = f'{self.train_origin}data'
        self.validation_target = config.get('validation_target', 'SR1F')
        self.validation_origin = config.get('validation_origin', 'CR1F')
        self.validation_target = f'{self.validation_target}data'
        self.validation_origin = f'{self.validation_origin}data'
        self.apply_target = config.get('apply_target', ['SR0F'])
        self.apply_origin = config.get('apply_origin', ['CR0F'])
        if self.config['rw_strategy']:
            self.apply_target = [f'{i}data' for i in self.apply_target]
            self.apply_origin = [f'{i}data' for i in self.apply_origin]
 #       else:
 #           self.apply_target = f'{self.apply_target}data' 
 #           self.apply_origin = f'{self.apply_origin}data' 
        self.config['train_target'] = self.train_target
        self.config['train_origin'] = self.train_origin
        self.config['validation_target'] = self.validation_target
        self.config['validation_origin'] = self.validation_origin
        self.config['apply_target'] = self.apply_target
        self.config['apply_origin'] = self.apply_origin
        self.config.pop('data_map')
        output_dir = config.get('output_dir', './')
        output_sub_dir = config.get('output_sub_dir', './')
        self.output_dir = path.join(output_dir, output_sub_dir)
        makedirs(self.output_dir, exist_ok=True)

        self.models = {}
        self.scalers = {}
        self.seeds = []
        self.yhat_tests = {}
        self.history = {}

    def setup_data(self, X_train, Y_train, X_val, Y_val, seed=0):
        self.X_train = X_train
        self.Y_train = Y_train
        self.X_val = X_val
        self.Y_val = Y_val
        self.seed = seed
        if seed in self.seeds:
            print('Warn seed {seed} is in {self.seeds}')
        self.seeds.append(seed)
        environ['PYTHONHASHSEED']=str(seed + 1234)
        import random
        random.seed(seed + 234)
        np.random.seed(seed + 34)
        from tensorflow.compat.v1 import set_random_seed
        set_random_seed(seed + 4)
        self.W_train = np.random.poisson(np.ones(self.X_train.shape[0])) if self.bootstrap else None
        if self.bootstrap_reserve_norm:
            self.W_train = self.W_train / np.sum(self.W_train) * self.X_train.shape[0]
        model_name = f'models/model_{self.seed}.h5'
        self.model_path = path.join(self.output_dir, model_name)

    def preprocess(self):
        fname = f'{self.output_dir}/scaler/scaler_{self.seed}.pkl'
        if self.cache and path.isfile(fname):
            with open(fname, 'rb') as pkl:
                self.scalers[self.seed] = pickle.load(pkl, encoding='bytes')
            print('skip preprocess')
        else:
            self.scalers[self.seed] = StandardScaler()
            self.X_train = self.scalers[self.seed].fit_transform(self.X_train)
            self.X_val = self.scalers[self.seed].transform(self.X_val)
            makedirs(path.dirname(fname), exist_ok=True)
            with open(fname, 'wb') as pkl:
                pickle.dump(self.scalers[self.seed], pkl)

    def louppe_loss(self, y_true, y_pred):
        # reference: https://github.com/glouppe/notebooks/blob/master/Direct%20density%20ratio%20estimation%20with%20backpropagation.ipynb
        return (y_true * (tf.math.sqrt(tf.math.exp(y_pred))) + (1.0 - y_true) * (1.0 / tf.math.sqrt(tf.math.exp(y_pred))))
        #return  ((y_true * (tf.math.exp(y_pred) **2)) - (1.0 - y_true) * (2.0 * tf.math.exp(y_pred)))

    def create_model(self, size, nnode):
        if self.cache and path.isfile(f'{self.model_path}'):
            print('skip create model, load from', self.model_path)
            model = load_model(f'{self.model_path}', custom_objects={'louppe_loss': self.louppe_loss})
        else:
            inputs = Input(shape=(size,))
            hidden = Dense(nnode, activation='relu', kernel_initializer=tf.keras.initializers.GlorotNormal())(inputs)
            hidden = Dense(nnode, activation='relu', kernel_initializer=tf.keras.initializers.GlorotNormal())(hidden)
            hidden = Dense(nnode, activation='relu', kernel_initializer=tf.keras.initializers.GlorotNormal())(hidden)
            hidden = Dense(nnode, activation='sigmoid', kernel_initializer=tf.keras.initializers.GlorotNormal())(hidden)
            outputs = Dense(1, activation='linear')(hidden)
            model = Model(inputs, outputs)
    
            adam = tf.keras.optimizers.Adam()
            model.compile(loss=self.louppe_loss, optimizer=adam)
            model.summary()
        self.models[self.seed] = model

    def train_model(self, patience=10, epoch=10):
        if self.cache and path.isfile(f'{self.model_path}'):
            return
        print('class weight', {0:self.train_target_class_weight, 1:1})
        self.history[self.seed] = self.models[self.seed].fit(self.X_train, self.Y_train, shuffle=True, verbose=1, sample_weight=self.W_train,
                    class_weight = {0:self.train_target_class_weight, 1:1},
                    callbacks = [EarlyStopping(monitor='val_loss', patience=patience, verbose=False),
                                 ModelCheckpoint(self.model_path, monitor='val_loss', verbose=False, save_best_only=True) ],
                    epochs=epoch,
                    validation_data=(self.X_val, self.Y_val),
                    batch_size=5000,)

    def plot_loss(self, start=0, batch=False):
        json_name = f'{self.output_dir}/loss/loss_allseed.json'
        
        if self.cache and path.isfile(json_name):
            with open(json_name, 'r') as fp:
                history = json.load(fp)
            history = {int(k):v for k,v in history.items()}
        else:
            history = {}
            print(self.seeds)
            print(self.history[self.seed].history)
            for i in self.history:
                print(i)
            for seed in self.seeds:
                history[int(seed)] = self.history[seed].history

            makedirs(path.dirname(json_name), exist_ok=True)

            os.makedirs(f'{self.output_dir}/loss')
            with open(json_name, 'w') as fp:
                json.dump(history, fp, indent=2)
                print('Save to ', json_name)
        
        plt.figure()
        for seed in history:
            plt.plot(history[seed]['loss'][start:], label=f'Train {seed}')
            plt.plot(history[seed]['val_loss'][start:], label=f'Val {seed}')
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.yscale('log')
        plt.legend(loc='upper right')
        plt.savefig(f'{self.output_dir}/loss/seed1.pdf')
        if not batch:
            plt.show()

        
    def apply_to_df(self, precuts=None, processes=None, write_to_root=False):
        print(self.apply_origin)
        if processes is None:
            processes = set([self.train_origin, self.validation_origin] + self.apply_origin)   
        if precuts is None:
            precuts = set([self.train_which_precut, self.apply_which_precut])
        print('Apply to', processes)
        print(precuts)
        for process in processes:
            for precut in precuts:
                fname = f'{self.output_dir}/Apply/pickle/{precut+"_" if len(precut)>0 else precut}{process}.feather'
                if self.cache and path.isfile(fname):
                    self.bkgmodel_df[precut][process] = pd.read_feather(fname)
                    print('Load', fname)
                else:
                    X_test = self.bkgmodel_df[precut][process][self.train_columns]
                    yhat_test = self.apply_all_seed(X_test)
                    self.bkgmodel_df[precut][process].loc[:, 'totalWeightBoosted_Smedian'] = yhat_test
                    for seed in self.seeds:
                        self.bkgmodel_df[precut][process].loc[:, f'totalWeightBoosted_S{seed}'] = self.yhat_tests[seed]
                    makedirs(path.dirname(fname), exist_ok=True)
                    self.bkgmodel_df[precut][process].reset_index(drop=True, inplace=True)
                    self.bkgmodel_df[precut][process] = self.bkgmodel_df[precut][process].astype(
                                                    dict([(j, 'float32') for j in [i for i in self.bkgmodel_df[precut][process] if i.startswith('totalWeightBoosted')]])
                    )
                    self.bkgmodel_df[precut][process] = self.bkgmodel_df[precut][process].astype(
                                                    dict([(j, 'int16') for j in [i for i in self.bkgmodel_df[precut][process] if i.startswith('HbbWP_h')]])
                    )
                    self.bkgmodel_df[precut][process].to_feather(fname)
                    print('Save to', fname)
                if write_to_root:
                    fname = f'{self.output_dir}/Apply/Root/{precut+"_" if len(precut)>0 else precut}{process}.root'
                    if self.cache and path.isfile(fname) and stat(fname).st_size > 0:
                        print('Skip', fname)
                        continue
                    else:
                        makedirs(path.dirname(fname), exist_ok=True)
                        dataframe2root(df=self.bkgmodel_df[precut][process], fname=fname, treename='fullmassplane')
                        print('Save to', fname)
                plt.hist(self.bkgmodel_df[precut][process]['totalWeightBoosted_Smedian'], bins=30, alpha=0.3, label=f'{precut+"-" if len(precut)>0 else precut}{process}')

    @staticmethod
    def create_hist(frame, var, raw_bins, weight, stat_type: str='Poisson', scale=1.0):
        # https://ddavis.io/posts/numpy-histograms/
        use_bins = [np.array([-np.inf]), raw_bins, np.array([np.inf])]
        use_bins = np.concatenate(use_bins)
        n, bins = np.histogram(frame[var], weights = frame[weight] * scale, bins=use_bins)
        n[1]  += n[0]   ## add underflow to first bin
        n[-2] += n[-1]  ## add overflow to last bin
        n = n[1:-1]     ## chop off the under/overflow
        bins = raw_bins
        if stat_type == None:
            return n
    
        def Poisson_error():
            # len(bins) = 41 => number of bin boundaries
            bin_sumw2 = np.zeros(len(bins) + 1, dtype=np.float32)
            digits = np.digitize(frame[var], raw_bins)
            for i in range(len(bins) + 1):
                series = frame[weight].reset_index(drop=True)[np.where(digits == i)[0]]
                bin_sumw2[i] = np.sum(np.power(series, 2)) if not series.empty else 0.
            err = bin_sumw2[1:-1]
            err[0] += bin_sumw2[0]
            err[-1] += bin_sumw2[-1]
            err = np.sqrt(err)
            return err
    
        if stat_type.lower() == 'poisson': # Poisson error
            err = Poisson_error()
    
        return n, err

    @staticmethod
    def assemble(outfile, hpx, n, err, print_yields=False):
        for i in range(1, hpx.GetNbinsX()+1):
            hpx.SetBinContent(i, n[i-1])
            hpx.SetBinError(i, err[i-1])
        outfile.cd()
        hpx.Write()

    def save_template(self, precuts=None, processes=None):
        from ROOT import TFile, TH1F
        from array import array

        def get_binning(raw_bins, bin_contents, error, threshold):
            rev_contents = bin_contents[::-1]
            rev_error = error[::-1]
            bins = [0]
            for i in range(len(rev_contents)):
                if rev_error[bins[-1]:i+1].sum() / rev_contents[bins[-1]:i+1].sum() >= threshold:
                    print(i, rev_error[bins[-1]:i+1].sum() / rev_contents[bins[-1]:i+1].sum(), threshold)
                    continue
                bins.append(i+1)
            return np.take(raw_bins[::-1], bins)[::-1]

        def loop(rw_strategy, precut, process, var, raw_bins):
            if 'kvv' in process:
                weight = 'totalWeightBoosted' 
            else:
                weight = 'totalWeightBoosted_Smedian'
            bin_contents, err = self.create_hist(frame = self.bkgmodel_df[precut][process], var = var, raw_bins = raw_bins, weight = weight)
            if 'kvv' not in process:
                bootstrap_weights = [i for i in self.bkgmodel_df[precut][process].columns if i.startswith('totalWeightBoosted_S') and i != weight]
                bootstrap_contents = []
                for bootstrap_weight in bootstrap_weights:
                    bootstrap_contents.append(self.create_hist(frame = self.bkgmodel_df[precut][process], var = var, raw_bins = raw_bins, stat_type = None, weight = bootstrap_weight))
                bootstrap_contents = np.array(bootstrap_contents)
                bootstrap_err = (np.quantile(bootstrap_contents, 0.84, axis=0) - np.quantile(bootstrap_contents, 0.16, axis=0))/2 # half of the 0.68 quantile as one side error
                err = np.sqrt(err*err + bootstrap_err*bootstrap_err)
            return bin_contents, err

        if processes is None:
            if self.rw_strategy != 5:
                processes = set([f'{target.replace("data", "").replace("CR", "SR")}kvv{kvv}' for target in self.apply_target for kvv in ('0', '0p5', '1', '1p5', '2', '3')] + [i for i in self.apply_origin])
            elif self.rw_strategy == 5:
                processes = set(['SR1FLodata','SR0FLokvv0','SR0FLokvv0p5','SR0FLokvv1','SR0FLokvv1p5','SR0FLokvv2','SR0FLokvv3'])
        if precuts is None:
            precuts = set([self.apply_which_precut])
        print('Apply to', processes)
        binning = {
            #'m_hhBoosted': [800, 1200, 1600, 2000]
            'm_hhBoosted': (12, 800, 2000),
        }

        var = 'm_hhBoosted'
        for precut in precuts:
            if type(binning[var]) == list:
                raw_bins = array('f', binning[var])
            elif type(binning[var]) == tuple:
                nbin, lo, hi = binning[var]
                raw_bins = np.linspace(lo, hi, nbin+1)
            fname = f'{self.output_dir}/Apply/Hist/{precut+"_" if len(precut)>0 else precut}hist.root'
            makedirs(path.dirname(fname), exist_ok=True)
            outfile = TFile(fname, 'recreate')
            
            for process in processes:
                bin_contents, err = loop(self.rw_strategy, precut, process, var, raw_bins)
                if 'kvv' not in process:
                    result = {
                        'bin_left': raw_bins[:-1],
                        'bin_right': raw_bins[1:],
                        'content': bin_contents,
                        'error': err,
                        'rel_err': err/bin_contents,
                    }
                    pd.DataFrame(result).to_csv(path.dirname(fname) + f'/{process}.csv')

                hpx = TH1F( f'{process}_{precut}__{var}', f'{process}_{precut}__{var}', len(raw_bins)-1, raw_bins )
                if process in ['SR0Fkvv1', 'SR0FHPkvv1', 'SR0FMPkvv1', 'SR0FTikvv1', 'SR0FLokvv1']:
                    print(f'\033[92mScale {process} by 1000\033[0m')
                    bin_contents *= 1000
                    err *= 1000
                self.assemble(outfile, hpx, bin_contents, err)

            print('Save templates to', f'{self.output_dir}/Apply/Hist/{precut+"_" if len(precut)>0 else precut}hist.root')


    def apply_all_seed(self, X_test=None):
        self.yhat_tests = {}
        for seed in self.seeds:
            self.yhat_tests[seed] = self.apply_single_seed(X_test, seed)
        return np.median(list(self.yhat_tests.values()), axis=0)

    def apply_single_seed(self, X_test=None, seed=0):
        if len(self.scalers) > 0:
            X_test = self.scalers[seed].transform(X_test.values)

        model_name = path.join(self.output_dir, f'models/model_{seed}.h5')
        model = load_model(f'{model_name}', custom_objects={'louppe_loss': self.louppe_loss})
        yhat_test = model(X_test)
        yhat_test = np.exp(yhat_test) / self.train_target_class_weight
        return yhat_test


    def train_bkg_reweighter(self):
        X_class1 = self.bkgmodel_df[self.train_which_precut][self.train_target][self.train_columns]
        X_class2 = self.bkgmodel_df[self.train_which_precut][self.train_origin][self.train_columns]
        self.train_target_class_weight = X_class2.shape[0] // X_class1.shape[0] / 10
        self.config['train_target_class_weight'] = self.train_target_class_weight
        print('Train target size', X_class1.shape[0], 'origin size', X_class2.shape[0])
        X_all, Y_all = self.get_X_Y(X_class1, X_class2) # target, original
        
        for seed in np.arange(self.nseeds):
            if self.iseed is not None and seed != self.iseed: continue
            X_train, X_val, Y_train, Y_val = train_test_split(X_all, Y_all, train_size=self.train_val_split, random_state=seed+self.base_seed)
            self.setup_data(X_train=X_train, Y_train=Y_train, X_val=X_val, Y_val=Y_val, seed=seed+self.base_seed)
            self.preprocess()
            if 'noVBFcut' in self.train_which_precut:
                nnode = 100
            else:
                nnode = 10
            self.create_model(X_train.shape[1], nnode=nnode)
            print('About to train seed', seed)
            self.train_model(patience=self.patience, epoch=self.epochs,)
        #self.plot_loss()
    def get_X_Y(self, X_class1, X_class2):
        X_train = pd.concat((X_class1, X_class2), ignore_index=True).values
        Y_train = np.concatenate((np.zeros(X_class1.shape[0]), np.ones(X_class2.shape[0])), axis=0)
        return X_train, Y_train

    def reweighted_yields(self):
        samples_and_weights = [
            ('!', None),
            ('NonSR0Fdata', 'totalWeightBoosted'),
            ('CR0Fdata', 'totalWeightBoosted'),
            ('!', None),
            ('SR0Fkvv0', 'totalWeightBoosted'),
            ('NonSR0Fkvv0', 'totalWeightBoosted'),
            ('CR0Fkvv0', 'totalWeightBoosted'),
            ('+', [(self.train_target, 'totalWeightBoosted')]),
            (self.train_target, 'totalWeightBoosted'),
            (self.train_origin, 'totalWeightBoosted_Smedian'),
   #         ('+', [(self.validation_target, 'totalWeightBoosted')]),
  #          (self.validation_target, 'totalWeightBoosted'),
   #         (self.validation_origin, f'totalWeightBoosted_Smedian'),
    #        ('+',[(self.apply_target[0], 'totalWeightBoosted')]),
     #       (self.apply_target[0], 'totalWeightBoosted'),
    #        (self.apply_origin[0], f'totalWeightBoosted_Smedian'),
        ]
       # samples_and_weights.extend([(self.validation_origin, f'totalWeightBoosted_S{seed}') for seed in self.seeds])
        samples_and_weights.extend([(self.train_origin, f'totalWeightBoosted_S{seed}') for seed in self.seeds])
        self.yields_result = {}
        for key in set([self.train_which_precut, self.apply_which_precut]):
            self.yields_result[key] = yields_table(self.bkgmodel_df[key], header=key, samples_and_weights=samples_and_weights)


    def reweighted_plots(self, additional_variables=[], batch=False, which_dataset='apply'):

       # if 'apply' in which_dataset:
       #     i = int(which_dataset[-1])
       #     target = self.apply_target[i]
       #     origin = self.apply_origin[i]
        if which_dataset == 'validate':
            target = self.validation_target
            origin = self.validation_origin
        elif which_dataset == 'train':
            target = self.train_target
            origin = self.train_origin
        elif which_dataset == 'apply':
            target = self.apply_target[0]
            origin = self.apply_origin[0]
        else:
            assert(0), 'support which_dataset = apply0|apply1|validate|train'

        plot_1D_options = {
            target: {
                'samples': [target],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'{target}',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[0],
                }
            },
            origin: {
                'samples': [origin],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'{origin}',
                    'linewidth': 4,
                    'linestyle': '--',
                    'color': color[1],
                }
            },
            f'{origin} (reweighted)': {
                'samples': [origin],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted_Smedian',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'{origin}\nreweighted',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[2],
                }
            },
        }

        plot_variables = self.train_columns + [i for i in additional_variables if i not in self.train_columns]
        self.chi2_df = {}
        for key in set([self.train_which_precut, self.apply_which_precut]):
            self.chi2_df[key] = plot_1D(self.bkgmodel_df[key], plot_variables=plot_variables, plot_options=plot_1D_options, extra_text=key+" "+which_dataset, output=path.join(self.output_dir), batch=batch, legend_loc=(0.6, 0.95-0.07*len(plot_1D_options)), run_chi2=True).chi2_results
            
            
    def reweighted_plots_witherror(self, additional_variables=[], batch=False, which_dataset='validate'):

        if 'apply' in which_dataset:
            i = int(which_dataset[-1])
            target = self.apply_target[i]
            origin = self.apply_origin[i]
        elif which_dataset == 'validate':
            target = self.validation_target
            origin = self.validation_origin
        elif which_dataset == 'train':
            target = self.train_target
            origin = self.train_origin
        else:
            assert(0), 'support which_dataset = apply0|apply1|validate|train'
        label = {
            'SR1FLodata': 'SR 1F Lo Data',
            'VR1FLodata': 'VR 1F Lo Data',
            'CR1FLodata': 'CR 1F Lo Data',
            'SR0FLodata': 'SR 0F Lo Data',
            'VR0FLodata': 'VR 0F Lo Data',
            'CR0FLodata': 'CR 0F Lo Data',
            'SR1Fdata': 'SR 1F  Data',
            'VR1Fdata': 'VR 1F  Data',
            'CR1Fdata': 'CR 1F  Data',
            'SR0Fdata': 'SR 0F  Data',
            'VR0Fdata': 'VR 0F  Data',
            'CR0Fdata': 'CR 0F  Data',
        }

        plot_variables = self.train_columns + [i for i in additional_variables if i not in self.train_columns]
        self.chi2_df = {}
        for key in set([self.train_which_precut, self.apply_which_precut]):
            df_map = {
                target: self.bkgmodel_df[key][target],
                origin: self.bkgmodel_df[key][origin],
                origin+'afterweight': self.bkgmodel_df[key][origin],
            }
            label_map = {
                target: label.get(target, target),
                origin: label.get(origin, origin),
                origin+'afterweight': label.get(origin, origin),
            }
            weight_map = {
                target: "totalWeightBoosted",
                origin: "totalWeightBoosted",
                origin+'afterweight': "totalWeightBoosted_Smedian",
            }
            comparison_options = {
                "reference": target,
                "target": [origin, origin+'afterweight'],
                "mode": "ratio",
                "label": "Ratio",
                "labelsize": 15
            }
            plot_1D_with_errorbar(df_map, label_map, plot_variables=plot_variables, comparison_options=comparison_options, weight=weight_map, extra_text=key+" "+which_dataset, output=path.join(self.output_dir), batch=batch, legend_loc=(0.6, 0.95-0.12*len(df_map)))


    def save(self, fname, chi2=True):

        all_results = {
            'config': self.config,
            'yields_table': self.yields_result,
        }
        if chi2:
            all_results['chi2'] = self.chi2_df
        self.save_json = f'{self.output_dir}/{fname}{self.nseeds}.json'
        with open(self.save_json, 'w') as fp:
            json.dump(all_results, fp, indent=2)
        print('Save to', f'{self.save_json}')

    def plot_bootstrap(self, precut='', plot_variables = 'm_hhBoosted', sample='CR1Fdata', label='Data CR1F', references=[], plot=True):
        plot_1D_options = {}
        for seed in self.seeds:
            plot_1D_options[f'{sample} S{seed}'] = {
                'samples': [sample],
                'type': 'norm',
                'weight_name': f'totalWeightBoosted_S{seed}',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    #'label': label+f' S{seed}',
                    'linewidth': 2,
                    'linestyle': '--',
                }
            }
    
        plotter = plot_1D(self.bkgmodel_df[precut], [plot_variables], plot_1D_options, extra_text = label, batch=False, draw_legend=True)
        central_value = np.median(np.array(plotter.hists), axis=0)
        print('seed', np.sum(np.square(np.array(plotter.hists)-central_value), axis=1).argsort()[::-1])
        bootstrap_err = (np.quantile(np.array(plotter.hists), 0.84, axis=0) - np.quantile(np.array(plotter.hists), 0.16, axis=0))/2 # half of the 0.68 quantile as one side error
        if not plot:
            return bootstrap_err

        x = plotter.bins[0]
        dic = {
            plot_variables: (x[1:]+x[:-1])/2,
            'totalWeightBoosted_hist': central_value
        }
    
        self.df_bootstrap = {}
        self.df_bootstrap[sample+'_histmedian'] = pd.DataFrame.from_dict(dic)
        self.df_bootstrap[sample+'_eventmedian'] = self.bkgmodel_df[precut][sample]
        for s,l in references:
            self.df_bootstrap[s] = self.bkgmodel_df[precut][s]
        
        i=0
        plot_1D_options = {}
        for s,l in references:
            plot_1D_options[l] = {
                'samples': [s],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': l,
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[i],
                }
            }
            i += 1
        plot_1D_options[sample+'_histmedian'] = {
            'samples': [sample+'_histmedian'],
            'type': 'norm',
            'weight_name': 'totalWeightBoosted_hist',
            "style": {
                'density': False,
                'alpha': 0.7,
                'histtype': 'step',
                'label': label + " hist median",
                'linewidth': 4,
                'linestyle': '-',
                'color': color[i],
            }
        }
        plot_1D_options[sample+'_eventmedian'] = {
            'samples': [sample+'_eventmedian'],
            'type': 'norm',
            'weight_name': 'totalWeightBoosted_Smedian',
            "style": {
                'density': False,
                'alpha': 0.7,
                'histtype': 'step',
                'label': label + " event median",
                'linewidth': 4,
                'linestyle': '-',
                'color': color[i+1],
            }
        }
    
        plotter = plot_1D(self.df_bootstrap, [plot_variables], plot_1D_options, extra_text = label, batch=True, legend_loc=(0.45, 0.95-0.07*len(plot_1D_options)))
        plotter.ax.fill_between(x[:-1], central_value - bootstrap_err, central_value + bootstrap_err,
            hatch="\\\\\\\\", facecolor="None", edgecolor=color[i+2], linewidth=1, step="post", zorder=1, label='Bootstrap error',)


    def plot_event_weight(self, process='CR1Fdata', num=9):
        indices = self.bkgmodel_df[''][process].filter(regex=("totalWeightBoosted_S2*")).T.columns[:num]
        nrow, ncol = int(np.sqrt(num)), int(np.ceil(num/np.sqrt(num)))
        f, axs = plt.subplots(ncol, nrow, figsize=(15, 15))
        for evt in range(len(indices)):
            i,j = int(evt/nrow), int(evt%nrow)
            axs[i, j].hist(self.bkgmodel_df[''][process].filter(regex=("totalWeightBoosted_S2*")).T[indices[evt]], bins=50, alpha=0.3,
                     )
        
    def plot_normalisation(self, process='CR1Fdata', which='weighted'):
        result_path = self.save_json
        with open(result_path) as fp:
            all_result = json.load(fp)
        norm, indices, norm_median = [], [], 0
        df = {}
        for k in all_result['yields_table']['']:
            if k.startswith(f'{process},2'):
                norm.append(all_result['yields_table'][''][k][which])
                indices.append(k.split(',')[-1])
            if k.startswith(f'{process},median'):
                norm_median = all_result['yields_table'][''][k][which]
        df[f'{process}__{which}'] = pd.DataFrame(norm, index=indices, columns =[f'{process}__{which}'])

        plot_1D_options = {}
        plot_1D_options[f'{process}__{which}'] = {
            'samples': [f'{process}__{which}'],
            'type': 'hist',
            "style": {
                'density': False,
                'alpha': 0.7,
                'histtype': 'step',
                'label': f"Median {norm_median:.3f}",
                'linewidth': 4,
                'linestyle': '-',
            }
        }
        analysis_label_options = {
            'status': 'Internal',
            'loc': (0.05, 0.95),
            'energy': '13 TeV',
            'lumi': r'139 fb$^{-1}$',
            'fontsize': 28,
            'extra_text': f'VBF 4b {process}',
        }
        styles = {
            'legend':{
                'loc': (0.6, 0.87),
                'fontsize': 22
            },
            'axis':{
                'tick_bothsides': True,
                'major_length': 12,
                'labelsize': 25,
            },
            'xlabel': {
                'fontsize': 30,
            },
            'ylabel': {
                'fontsize': 30,
            },
        }
    
        from .score_distribution_plot import myScoreDistributionPlot
        plotter = myScoreDistributionPlot(df, plot_options=plot_1D_options, 
                                    analysis_label_options=analysis_label_options, styles=styles)
        plotter.run_chi2(False)
        plotter.draw(column_name=f'{process}__{which}', xmin=0.6, xmax=1.4, xlabel='Norm', boundaries=[norm_median])

