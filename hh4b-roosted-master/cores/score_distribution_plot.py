from typing import Optional, Union, Dict, List
from pdb import set_trace

import pandas as pd
import numpy as np
from scipy import stats

from matplotlib import colors
from matplotlib.ticker import MaxNLocator
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon

from quickstats.plots import AbstractPlot
from quickstats.plots.template import single_frame, parse_styles, create_transform, format_axis_ticks
from quickstats.utils.common_utils import combine_dict
from quickstats.plots import ScoreDistributionPlot


class myScoreDistributionPlot(ScoreDistributionPlot):
    def run_chi2(self, chi2=True):
        self.chi2 = chi2
        self.chi2_results = {}
    
    def draw(self, column_name:str="score",
             xlabel:str="Score", ylabel:str="Fraction of Events / {bin_width:.2f}",
             boundaries:Optional[List]=None, nbins:int=40, xmin:float=0, xmax:float=1, draw_legend=True):
        """
        
        Arguments:
            column_name: string, default = "score"
                Name of the score variable in the dataframe.
            xlabel: string, default = "Score"
                Label of x-axis.
            ylabel: string, default = "Fraction of Events / {bin_width}"
                Label of y-axis.
            boundaries: (optional) list of float
                If specified, draw score boundaries at given values.
            nbins: int, default = 25
                Number of histogram bins.
            xmin: float, default = 0
                Minimum value of x-axis.
            xmax: float, default = 1
                Maximum value of x-axis.
        """
        ax = self.draw_frame()
        self.hists = []
        self.bins = []
        self.samps = []
        to_norm = None # get the first total norm from plot_option
        for key in self.plot_options:
            samples = self.plot_options[key]["samples"]
            plot_style  = self.plot_options[key].get("style", {})
            weight_name = self.plot_options[key].get("weight_name", None)
            df = pd.concat([self.data_map[sample] for sample in samples], ignore_index = True)
            plot_type = self.plot_options[key].get("type", "hist")
            if plot_type == "hist":
                if weight_name is not None:
                    norm_weights = df[weight_name]
                else:
                    norm_weights = None
                y, x, _ = ax.hist(df[column_name], nbins, range=(xmin, xmax),
                                  weights=norm_weights, **plot_style, zorder=-5)
                self.hists.append(y)
                self.bins.append(x)
                self.samps.append(key)
            elif plot_type == "norm":
                if weight_name is not None:
                    norm_weights = df[weight_name]
                    if to_norm is None:
                        to_norm = norm_weights.sum()
                    else:
                        norm_weights = norm_weights * to_norm / norm_weights.sum()
                else:
                    if to_norm is None:
                        to_norm = df.shape[0]
                    else:
                        norm_weights = to_norm / df.shape[0]
                y, x, _ = ax.hist(df[column_name], nbins, range=(xmin, xmax),
                                  weights=norm_weights, **plot_style, zorder=-5)
                self.hists.append(y)
                self.bins.append(x)
                self.samps.append(key)
            elif plot_type == "errorbar":
                n_data = len(df[column_name])
                norm_weights = np.ones((n_data,)) / n_data
                y, bins = np.histogram(df[column_name], nbins, weights=norm_weights)
                bin_centers  = 0.5*(bins[1:] + bins[:-1])
                from quickstats.maths.statistics import get_poisson_interval
                yerr = get_poisson_interval(y * n_data)
                ax.errorbar(bin_centers, y, 
                            yerr=(yerr["lo"] / n_data, yerr["hi"] / n_data),
                            **plot_style)
            else:
                raise RuntimeError(f'unknown plot type: {plot_type}')
                
        if self.chi2:
            self.chi2_results[column_name] = {}
            for samp, nback in zip(self.samps[1:], self.hists[1:]):
                chi2_result = self.weighted_chisquare(self.hists[0], nback, np.sqrt(self.hists[0]), np.sqrt(nback))
                chi2_results_reduced = chi2_result['X2']/chi2_result['ndf']
                self.chi2_results[column_name][f'{self.samps[0]} vs {samp}'] = float(chi2_results_reduced)
            reweighted_key = [k for k in self.samps if 'reweighted' in k]
            unreweighted_key = [k.replace(' (reweighted)', '') for k in reweighted_key]
            text_extra = ax.text(0.99, 1.02, r'$\chi^2/ndf: {:.2f} \rightarrow {:.2f}$'.format(self.chi2_results[column_name][f'{self.samps[0]} vs {unreweighted_key[0]}'], self.chi2_results[column_name][f'{self.samps[0]} vs {reweighted_key[0]}']), fontsize=25, transform=ax.transAxes, horizontalalignment='right', verticalalignment='bottom')

        bin_width = (xmax - xmin) / nbins
        ylabel = ylabel.format(bin_width=bin_width)
        
        self.draw_axis_components(ax, xlabel=xlabel, ylabel=ylabel)
        self.set_axis_range(ax, xmin=xmin, xmax=xmax)
        
        ax.yaxis.set_major_locator(MaxNLocator(prune='lower', steps=[10]))
        ax.xaxis.set_major_locator(MaxNLocator(steps=[10]))

        handles, labels = ax.get_legend_handles_labels()
        new_handles = [Line2D([], [], c=h.get_edgecolor(), linestyle=h.get_linestyle(),
                              **self.styles['legend_Line2D']) if isinstance(h, Polygon) else h for h in handles]
        if draw_legend:
            ax.legend(handles=new_handles, labels=labels, **self.styles['legend'])
        if boundaries is not None:
            for boundary in boundaries:
                ax.axvline(x=boundary, **self.config["boundary_style"])
        self.ax = ax


    def weighted_chisquare(self, f_obs: np.array, f_exp: np.array, f_obs_err: np.array, f_exp_err: np.array, stat_lim: int = 1,):
        """
        Calculate weighted chi^2 between hists using method from arXiv:physics/0605123, ignoring low stat bins
    
        Parameters
        ----------
        f_obs : np.array
            Histogram of observed events
    
        f_exp : np.array
            Histogram of expected events
    
        f_obs_err : np.array
            Uncertainty in observed histogram
    
        f_exp_err : np.array
            Uncertainty in expected histogram
    
        stat_lim : int
            Minimum entries for a bin to be included in the calculation
    
        return : dict
            Dictionary containing results and info from the chi2 calculation (X2, p_val, ndf, residuals)
        """
    
        statMask = (f_obs >= stat_lim) | (f_exp >= stat_lim)
        w1, w2 = f_obs[statMask], f_exp[statMask]
        s1, s2 = f_obs_err[statMask], f_exp_err[statMask]  # noqa
    
        # Calculate weighted chi-square using method in arXiv:physics/0605123
        ndf = len(w1) - 1
        W1, W2 = np.sum(w1), np.sum(w2)  # noqa
    
        R = np.nan_to_num(
            (W1 * w2 - W2 * w1) ** 2 / (W1 ** 2 * s2 ** 2 + W2 ** 2 * s1 ** 2),
            posinf=0,
            neginf=0,
        )
    
        X2 = np.sum(R)
        p_value = stats.chi2.sf(X2, ndf)
        return {"X2": X2, "p_val": p_value, "ndf": ndf}
    
        ## Calculate residuals
        #pi = (w1 * W1 / (s1 ** 2)) + (w2 * W2 / (s2 ** 2)) / (
        #    ((W1 ** 2) / (s1 ** 2)) + ((W2 ** 2) / (s2 ** 2))
        #)
    
        #frac = 1 + ((W2 ** 2) * (s1 ** 2)) / ((W1 ** 2) / (s2 ** 2))
        #top = w1 - W1 * pi
        #bottom = s1 * np.sqrt(1 - 1 / frac)
        #ri = top / bottom
    
        #return {"X2": X2, "p_val": p_value, "ndf": ndf, "residuals": ri}
    
