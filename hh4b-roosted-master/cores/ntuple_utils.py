from quickstats.components.processors import RooProcessor
from quickstats.utils.common_utils import combine_dict
from os import path, makedirs
from glob import glob
import pandas as pd
import pickle
from quickstats.utils.data_conversion import root2dataframe
from pdb import set_trace
import matplotlib.pyplot as plt
import numpy as np

def get_df(path, tree, variables, selections):
    if isinstance(path, list):
        files = []
        for i in path:
            files.extend(glob(i))
    else:
        files = glob(path)
    df = []
    assert(len(files) > 0), f'no files found in {path}'
    for ifile in files:
        frame = root2dataframe(ifile, tree, columns=variables, remove_non_standard_types=False)
        if 'mc16a' in ifile:
            lumi = 36.20766
        elif 'mc16d' in ifile:
            lumi = 44.3074
        elif 'mc16e' in ifile:
            lumi = 58.4501
        else:
            lumi = 1
        print('Loaded', ifile, tree, lumi, frame.shape[0])
        frame['lumi'] = lumi
        if selections:
            for var, selection in selections.items():
                if '|' in var:
                    vars = var.split('|')
                    vars = [i.replace(' ', '')  for i in vars]
                    if 'low' in selection:
                        frame = frame.loc[(frame[vars[0]] > selection['low']) | (frame[vars[1]] > selection['low'])]
                else:
                    if 'high' in selection:
                        frame = frame.loc[(frame[var] <= selection['high'])]
                    if 'low' in selection:
                        frame = frame.loc[(frame[var] > selection['low'])]
                    if 'equal' in selection:
                        frame = frame.loc[(frame[var] == selection['equal'])]
                    if 'exclude' in selection:
                        frame = frame.loc[(frame[var] != selection['exclude'])]
                print('\t- Pass', var, frame.shape[0])
        df.append(frame)

    try:
        df = pd.concat(df)
    except:
        set_trace()
    return df

def get_all_df(path_to_file, variables, selections=None):
    sample_df = {}

    #if path.exists(path_to_file['cache']) and input(f"Load from {path_to_file['cache']}? [y/n]").lower() == "y" and not batch_mode:
    if path.exists(path_to_file['cache']):
        print('Load from', path_to_file['cache'])
        with open(path_to_file['cache'], 'rb') as handle:
            sample_df = pickle.load(handle)
    else:
        for sample in path_to_file.keys():
            if sample == 'cache': continue
            sample_path = path_to_file[sample]
            sample_df[sample] = get_df(path=sample_path['orig'], tree=sample_path['tree'], variables=variables, selections=selections)

        makedirs(path.dirname(path_to_file['cache']), exist_ok=True)
        with open(path_to_file['cache'], 'wb') as handle:
            pickle.dump(sample_df, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print(f"Save to {path_to_file['cache']}")
    assert(sample_df)
    return sample_df



def process_ntuple(config, ntuple_to_process):

    paths = ntuple_to_process['orig']
    out = ntuple_to_process['rename']
    if isinstance(paths, list):
        files = []
        for i in paths:
            files.extend(glob(i))
    else:
        files = glob(paths)
    df = []
    assert(len(files) > 0), f'no files found in {paths}'
    processor = RooProcessor(config)
    for ifile in files:
        processor.global_variables['outfile'] = ifile.replace(out[0], out[1]+'/')
        if path.exists(processor.global_variables['outfile']) and input(f"Overwrite {processor.global_variables['outfile']}? [y/n]").lower() != "y":
            continue
        else:
            processor.run(ifile)
        

def load_ntuple(cache_path):
    pre_variables = [
            # 'kinematic_region', # Resolved SR=0, VR=1, CR=2
            # 'boostedSR', 
            'totalWeightBoosted', # 'mc_sf',
            'boostedPrecut',    # 
            'pt_h1Boosted',     # > 450 GeV
            'm_h1Boosted',      # > 50 GeV
            'pt_h2Boosted',     # > 250 GeV
            'm_h2Boosted',      # > 50 GeV
            'eta_VBFj1Boosted', # abs(Delta eta VBFjj) > 3
            'eta_VBFj2Boosted',
            'pt_VBFj1Boosted',  # > 20
            'pt_VBFj2Boosted',  # > 20
            'm_VBFjjBoosted',   # > 100
            'HbbWP_h1Boosted',  # <= 70
            'HbbWP_h2Boosted',  # <= 70
            'm_hhBoosted',
            'event_number',
            'eta_h1Boosted',
            'eta_h2Boosted',
            'passVBFJets',
            'passResolvedSR',
            'X_hhBoosted',
            'pt_hhBoosted',
            'eta_hhBoosted',
            'E_VBFj1Boosted',
            'E_VBFj2Boosted',

        ]
    
    SR_presel = {
        "passVBFJets": {
            "equal": 1
        },
    }
    
    path_to_ntuple_files = [
        {
            'data': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/NNT_2022-SEP-16/data1*.root',
                'tree': 'fullmassplane',
            },
            'cache': f'{cache_path}/df_hh4b_Data.pkl',
            'selection': SR_presel,
        },
        {
            'kvv1': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv1/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'kvv0': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv0/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'kvv0p5': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv0p5/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'kvv1p5': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv1p5/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'kvv2': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv2/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'kvv3': {
                'orig': '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/sig_ntuple/kvv3/NanoNTuple.root',
                'tree': 'fullmassplane',
            },
            'cache': f'{cache_path}/df_hh4b_sig.pkl',
            'selection': SR_presel,
    
        },
        
        #{
        #    'dijetbfilter': {
        #        'orig': '/eos/user/s/srettie/HH/NNT/VBFBOOSTED-SEP22-0/MC/80028?_mc16?/NanoNTuple.root',
        #        'tree': 'fullmassplane',
        #    },
        #    'cache': f'{cache_path}/df_hh4b_dijet.pkl',
        #    'selection': SR_presel,
    
        #},
        
  #      {
 #           'ttbarhtfilter': {
 #               'orig': ['/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/ttbarhtfilter/40734[2-4]_mc16?/NanoNTuple.root',
 #                        '/publicfs/atlas/atlasnew/hgtd/fengy/ntuple/ttbarhtfilter/4104??_mc16?/NanoNTuple.root',],
 #               'tree': 'fullmassplane',
 #           },
#            'cache': f'{cache_path}/df_hh4b_ttbar.pkl',
 #           'selection': SR_presel,
 #       },
    ]
    
    all_sample_dfs = {}
    for path_to_file in path_to_ntuple_files:
        selection = path_to_file.pop('selection', None)
        sample_dfs = get_all_df(path_to_file, pre_variables, selection)
        all_sample_dfs = combine_dict(all_sample_dfs, sample_dfs)
    return all_sample_dfs


def construct_variables(sample_df):
    for sample in sample_df:
        sample_df[sample].loc[:, 'delta_eta_VBFjjBoosted'] = abs(sample_df[sample]['eta_VBFj1Boosted'] - sample_df[sample]['eta_VBFj2Boosted'])
        sample_df[sample].loc[:, 'delta_ptHH_boosted'] = sample_df[sample]['pt_h1Boosted'] - sample_df[sample]['pt_h2Boosted']
        sample_df[sample].loc[:, 'log_delta_ptHH_boosted'] = np.log(sample_df[sample]['delta_ptHH_boosted'])
        sample_df[sample].loc[:, 'delta_etaHH_boosted'] = sample_df[sample]['eta_h1Boosted'] - sample_df[sample]['eta_h2Boosted']
        sample_df[sample].loc[:, 'log_pt_h1Boosted'] = np.log(sample_df[sample]['pt_h1Boosted'])
        sample_df[sample].loc[:, 'log_pt_h2Boosted'] = np.log(sample_df[sample]['pt_h2Boosted'])
        sample_df[sample].loc[:, 'log_pt_VBFj1Boosted'] = np.log(sample_df[sample]['pt_VBFj1Boosted'])
        sample_df[sample].loc[:, 'log_pt_VBFj2Boosted'] = np.log(sample_df[sample]['pt_VBFj2Boosted'])
        sample_df[sample].loc[:, 'log_pt_hhBoosted'] = np.log(sample_df[sample]['pt_hhBoosted'])
        sample_df[sample].loc[:, 'log_m_h1Boosted'] = np.log(sample_df[sample]['m_h1Boosted'])
        sample_df[sample].loc[:, 'log_m_h2Boosted'] = np.log(sample_df[sample]['m_h2Boosted'])
        sample_df[sample].loc[:, 'log_m_hhBoosted'] = np.log(sample_df[sample]['m_hhBoosted'])

        if 'lumi' in sample_df[sample]:
            sample_df[sample]['totalWeightBoosted'] *= sample_df[sample]['lumi']
            sample_df[sample].drop(['lumi'], axis=1, inplace=True)
        sample_df[sample]=sample_df[sample].astype('float32')
    return sample_df

def construct_variable_for_BDT(sample_df):
    for whichcut in sample_df:
        for sample in sample_df[whichcut]:
 #           sample_df[whichcut][sample].loc[:, 'One_Hot_encoder_Bit_1'] = [0 for x in range(sample_df[whichcut][sample].iloc[:,0].size)]
 #           sample_df[whichcut][sample].loc[:, 'One_Hot_encoder_Bit_2'] = [0 for x in range(sample_df[whichcut][sample].iloc[:,0].size)]
            sample_df[whichcut][sample]['One_Hot_encoder_Bit_1'] = sample_df[whichcut][sample]['event_number'].apply(lambda x:1 if x % 10 > 7 and (('1F' in sample) or ('0F' in sample)) else 0)           
            sample_df[whichcut][sample]['One_Hot_encoder_Bit_2'] = sample_df[whichcut][sample]['event_number'].apply(lambda x:1 if x % 10 <= 7 and (('1F' in sample) or ('0F' in sample)) else 0)

  #  for whichcut in ['']:
  #      for sample in sample_df[whichcut]:
  #          print('cut is:'+ whichcut)
  #          if (('1F' in sample) or ('0F' in sample)):
  #              print(sample +':')
  #              print (sample_df[whichcut][sample]['One_Hot_encoder_Bit_1'].sum() / sample_df[whichcut][sample].iloc[:,0].size)
  #              print (sample_df[whichcut][sample]['One_Hot_encoder_Bit_2'].sum() / sample_df[whichcut][sample].iloc[:,0].size)
    return sample_df



              
            
      
            
        
