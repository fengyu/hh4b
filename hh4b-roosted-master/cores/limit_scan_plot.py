from typing import Tuple, Dict, Optional, List, Union
import math
import json
import numpy as np
import pandas as pd

from quickstats.plots import UpperLimit2DPlot
from quickstats.utils.common_utils import combine_dict

#Now using values from LHCWHGHHHXGGBGGGXXX
#correct to xs at mH = 125.09
SCALE_GGF_KL  = 31.02/(70.3874 - 50.4111 + 11.0595)
SCALE_VBF_KL  = 1.723/(4.581 - 4.245 + 1.359)
SCALE_VBF_K2V = 1.723/(27.08 - 44.42 + 19.07)

def get_intersections(x_data, y_data, x_theory, y_theory):
    # get the intersection between expected and theory prediction
    
    # interpolate expected limit with same number of datapoints as used in theory prediction
    interpolated_limit = np.interp(x_theory, x_data, y_data) 

    #limitm1 = n*np.array(limit_bands[0]) - 1
    limitm1 = interpolated_limit - y_theory 
    idx = np.argwhere(np.diff(np.sign(limitm1))).flatten() # determines what index intersection points are at 

    #linear interpolation to get exact intercepts: x = x1 + (x2-x1)/(y2-y1) * (y-y1)
    #y = 0 -> x = x1 - (x2-x1)/(y2-y1) * y1
    intersections = [x_theory[x] - (x_theory[x+1] - x_theory[x])/(limitm1[x+1] - limitm1[x]) * limitm1[x] for x in idx]
    return intersections

class DiHiggsXS:
    def __init__(self, s:float=13, include_ggF:bool=True, include_VBF:bool=True):
        self.s = s
        self.include_ggF = include_ggF
        self.include_VBF = include_VBF
    
    @staticmethod
    def xs_ggF_kl(kl:float):
        #https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH#Latest_recommendations_for_gluon
        return (70.3874 - 50.4111 * kl + 11.0595 * kl**2) * SCALE_GGF_KL #XS in fb
    
    @staticmethod
    def xs_VBF_kl(kl:float):
        #https://indico.cern.ch/event/995807/contributions/4184798/attachments/2175756/3683303/VBFXSec.pdf
        return (4.581 - 4.245 * kl + 1.359 * kl**2) * SCALE_VBF_KL
    
    @staticmethod
    def xs_VBF_k2v(k2v:float):
        #https://indico.cern.ch/event/995807/contributions/4184798/attachments/2175756/3683303/VBFXSec.pdf
        return (27.08 - 44.42 * k2v + 19.07 * k2v**2) * SCALE_VBF_K2V
    
    def get_coupling_xs(self, coupling:str, value:np.ndarray):
        if coupling == "klambda":
            if self.s == 13:
                return self.include_ggF * self.xs_ggF_kl(value) + self.include_VBF * self.xs_VBF_kl(value)
            elif self.s == 14:
                return self.include_ggF * self.xs_ggF_kl(value)*1.18 + self.include_VBF * self.xs_VBF_kl(value) * 1.19
        elif coupling == "k2v":
            if self.s == 13:
                return self.include_ggF * self.xs_ggF_kl(1) + self.include_VBF * self.xs_VBF_k2v(value)
            elif self.s == 14:
                return self.include_ggF * self.xs_ggF_kl(1)*1.18 + self.include_VBF * self.xs_VBF_k2v(value)*1.19
        else:
            raise ValueERROR(f"no prediction for the coupling `{coupling}` available")
            
    # When adding 2 independent Gaussians (e.g. ggF and VBF XS) we can simply add their means and add their sigmas in quadrature
    def sigma_upper_ggF(self, kl:float):
        #https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH#Latest_recommendations_for_gluon
        #add the std on ggF HH due to qcd scale, PDF, and mtop in quadrature
        #return xs_ggF(kl) * math.sqrt((max(72.0744-51.7362*kl+11.3712*kl**2, 70.9286-51.5708*kl+11.4497*kl**2) * SCALE_GGF / xs_ggF(kl) - 1)**2 + 0.03**2 + 0.026**2)
        #new mtop uncertainty:
        return self.xs_ggF_kl(kl) * math.sqrt((max(76.6075 - 56.4818*kl + 12.635*kl**2, 75.4617 - 56.3164*kl + 12.7135*kl**2) * SCALE_GGF_KL / self.xs_ggF_kl(kl) - 1)**2 + 0.03**2)

    def sigma_lower_ggF(self, kl:float):
        #https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWGHH?redirectedfrom=LHCPhysics.LHCHXSWGHH#Latest_recommendations_for_gluon
        #add the std on ggF HH due to qcd scale, PDF, and mtop in quadrature
        #return xs_ggF(kl) * math.sqrt((min(66.0621-46.7458*kl+10.1673*kl**2, 66.7581-47.721*kl+10.4535*kl**2) * SCALE_GGF / xs_ggF(kl) - 1)**2 + 0.03**2 + 0.026**2)
        #new mtop uncertainty:
        return self.xs_ggF_kl(kl) * math.sqrt((min(57.6809 - 42.9905*kl + 9.58474*kl**2, 58.3769 - 43.9657*kl + 9.87094*kl**2) * SCALE_GGF_KL / self.xs_ggF_kl(kl) - 1)**2 + 0.03**2)

    def sigma_upper_VBF(self, kl:float):
        #from klambda = 1
        return self.xs_VBF_kl(kl) * math.sqrt(0.0003**2 + 0.021**2)

    def sigma_lower_VBF(self, kl:float):
        return self.xs_VBF_kl(kl) * math.sqrt(0.0004**2 + 0.021**2)

    def sigma_upper_HH(self, kl:float):
        error = math.sqrt(self.sigma_upper_ggF(kl)**2 + self.sigma_upper_VBF(kl)**2)
        if self.s == 14:
            error /= 2
        return error

    def sigma_lower_HH(self, kl:float):
        error = math.sqrt(self.sigma_lower_ggF(kl)**2 + self.sigma_lower_VBF(kl)**2)
        if self.s == 14:
            error /= 2
        return error

    def xs_upper_HH(self, kl:float):
        return self.get_coupling_xs("klambda", kl) + self.sigma_upper_HH(kl)

    def xs_lower_HH(self, kl:float):
        return self.get_coupling_xs("klambda", kl) - self.sigma_lower_HH(kl)
    
    def get_theory_curve(self, coupling:str, vmin:float, vmax:float):
        x = np.linspace(vmin, vmax, 1000) 
        y = self.get_coupling_xs(coupling, x)
        if coupling == "klambda":
            y_errlo = np.array([self.xs_lower_HH(v) for v in x])
            y_errhi = np.array([self.xs_upper_HH(v) for v in x])
        else:
            y_errlo = None
            y_errhi = None
        return x, y, y_errlo, y_errhi
    
    def get_intersections(self, coupling:str, coupling_values:np.ndarray, limit_values:np.ndarray):
        coupling_xs = self.get_coupling_xs(coupling, coupling_values)
        x, y, y_errlo, y_errhi = self.get_theory_curve(coupling, min(coupling_values), max(coupling_values))
        intersections = get_intersections(coupling_values, coupling_xs * limit_values, x, y)
        return intersections

class LimitScanPlot:
    def __init__(self, limit_path:str, param_name:str,
                 additional_limit:Optional[List[Dict]]=None,
                 labels:Optional[Dict]=None,
                 styles:Optional[Union[Dict, str]]=None,
                 analysis_label_options:Optional[Dict]=None):
        self.styles = styles
        self.analysis_label_options = analysis_label_options
        self.param_name = param_name
        self.limit_data = json.load(open(limit_path))
        self.df = pd.DataFrame(self.limit_data).set_index([param_name])
        self.additional_limit = []
        if additional_limit is not None:
            for config in additional_limit:
                config_new = combine_dict(config)
                filename = config_new.pop("filename", None)
                if filename is not None:
                    limit_data = json.load(open(filename))
                    df = pd.DataFrame(limit_data).set_index([param_name])
                    config_new["data"] = df
                self.additional_limit.append(config_new)
        self.labels = labels
        self.intersections = None
        
    def draw(self, model_name:str="", draw_observed:bool=False, vbf_only:bool=False, ylim:Tuple=(10, 1e5)):
        
        param_values = self.df.index.astype(float).values
        xs_maker = DiHiggsXS(s=13, include_ggF=not vbf_only, include_VBF=True)
        coupling_xs = np.array([xs_maker.get_coupling_xs(self.param_name, value) for value in param_values])
        x, y, y_errlo, y_errhi = xs_maker.get_theory_curve(self.param_name, min(param_values), max(param_values))

        plotter = UpperLimit2DPlot(self.df, additional_data=self.additional_limit,
                                   labels=self.labels,
                                   scale_factor=coupling_xs, styles=self.styles,
                                   analysis_label_options=self.analysis_label_options,
                                   config={"observed_plot_styles": {"marker": "None"}})
        plotter.add_curve(x, y, y_errlo, y_errhi,
                          label="Theory prediction")
        SM_point = xs_maker.get_coupling_xs(self.param_name, 1)
        plotter.add_highlight(1, SM_point,
                              label="SM prediction")
        
        if self.param_name == "klambda":
            param_latex = r"$\mathit{\kappa_{\lambda}}$"
        elif self.param_name == "k2v":
            param_latex = r"$\mathit{\kappa_{2V}}$"
            
        if vbf_only:
            ylabel = r"$\sigma_{VBF}(\mathit{HH})$ [fb]"
        else:
            ylabel = r"$\sigma_{ggF+VBF}(\mathit{HH})$ [fb]"
            
        ax = plotter.draw(xlabel=param_latex, ylabel=ylabel,
                          draw_observed=draw_observed, log=True, ylim=ylim, 
                          xlim=[min(param_values), max(param_values)],
                          draw_hatch=False)
        try:
            intersections_exp = get_intersections(param_values, 
                                                  coupling_xs*self.df['0'],
                                                  x, y)
            self.intersections = {
                "expected": intersections_exp
            }
            if draw_observed:
                intersections_obs = get_intersections(param_values, 
                                                      coupling_xs*self.df['obs'],
                                                      x, y)
                self.intersections['observed'] = intersections_obs
            if self.param_name == "klambda":
                param_latex = r"\kappa_{\lambda}"
            elif self.param_name == "k2v":
                param_latex = r"\kappa_{2V}"
            ax.annotate(r'Expected: $%s \in [%.2f, %.2f]$' %(param_latex, intersections_exp[0], intersections_exp[1]), 
                        (0.05, 0.08), xycoords = 'axes fraction', fontsize = 18)
            ax.annotate(f'Allowed range: {abs(intersections_exp[1] - intersections_exp[0]):.2f}', 
                        (0.05, 0.16), xycoords = 'axes fraction', fontsize = 18)
        except:
            pass
        return ax
