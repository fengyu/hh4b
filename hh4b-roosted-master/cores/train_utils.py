from os import path, makedirs
import pickle

def get_data_fold(plot_df=None):
    fold_path = '/tmp/zhangr/zhangr/df_hh4b_fold_{}.pkl'
    if path.exists(fold_path.format(3)) and input(f"Load from {fold_path}? [y/n]").lower() == "y":
        folds = []
        for i in range(4):
            with open(fold_path.format(i), 'rb') as handle:
                folds.append(pickle.load(handle))
    else:
        folds = [{} for i in range(4)]
        for sample in plot_df:
            for i, df in plot_df[sample].groupby(['fold']):
                folds[i][sample] = df

        makedirs(path.dirname(fold_path), exist_ok=True)
        for i in range(4):
            with open(fold_path.format(i), 'wb') as handle:
                pickle.dump(folds[i], handle, protocol=pickle.HIGHEST_PROTOCOL)
                print(f"Save to {fold_path.format(i)}")
    return folds


