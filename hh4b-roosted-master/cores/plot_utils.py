from typing import Optional, Union, Dict, List
import pandas as pd
import numpy as np
from .score_distribution_plot import myScoreDistributionPlot
from .pdf_distribution_plot import myPdfDistributionPlot
from quickstats.plots.template import single_frame, parse_styles, format_axis_ticks
from matplotlib import colors
from matplotlib.ticker import MaxNLocator
from os import makedirs
import matplotlib.pyplot as plt
from numpy import unravel_index
from scipy import interpolate, stats
import uproot
from pdb import set_trace

def opt_SR(cutflowobj, output):
    results = []
    for key in ['SRtest1', 'SRtest2', 'SRtest3']:
        for i in np.arange(1.6, 2.2, 0.1):
            for fm1 in np.arange(1200, 1700, 100):
                for fm2 in np.arange(1700, 2200, 100):
                    zvalue1 = cutflowobj.significance(fm1=fm1, fm2=fm2, radius=i, key=key,
                              signals=['kvv1', 'kvv0', 'kvv0p5', 'kvv1p5', 'kvv2', 'kvv3'], backgrounds=['dijetbfilter', 'ttbarhtfilter'])
                    zvalue2 = cutflowobj.significance(fm1=fm1, fm2=fm2, radius=i, key=key,
                              signals=['kvv0'], backgrounds=['dijetbfilter', 'ttbarhtfilter'])
                    results.append([key, pretty_value(i), fm1, fm2, zvalue1, zvalue2])
    df_zvalue = pd.DataFrame(results, columns=['function', 'radius', 'fm1', 'fm2', 'ZAall', 'ZA0'])
    makedirs(f'{output}/SRopt/', exist_ok=True)
    df_zvalue.to_csv(f'{output}/SRopt/ZAopt.csv')
    return df_zvalue


VARIABLES_1D = {
    'pt_h1Boosted': (r'$p_T (H_1)$ [GeV]', 400, 1000, 30),
    'pt_h2Boosted': (r'$p_T (H_2)$ [GeV]', 200, 1000, 40),
    'delta_ptHH_boosted': (r'$\Delta p_T (JJ)$', 0, 1000, 50),
    'log_delta_ptHH_boosted': (r'log$\Delta p_T (JJ)$', 0, 10, 20),
    'eta_h1Boosted': (r'$\eta (H_1)$', -2.5, 2.5, 25),
    'eta_h2Boosted': (r'$\eta (H_2)$', -2.5, 2.5, 25),
    'eta_hhBoosted': (r'$\eta_{HH}$', -5, 5, 20),
    'delta_etaHH_boosted':  (r'$\Delta \eta (JJ)$', -4, 4, 20),
    'm_h1Boosted':  (r'$m (H_1)$ [GeV]', 50, 200, 30),
    'm_h2Boosted':  (r'$m (H_2)$ [GeV]', 50, 200, 30),
    'log_m_h1Boosted':  (r'log($m (H_1)$)', 3.5, 6.5, 20),
    'log_m_h2Boosted':  (r'log($m (H_2)$)', 3.5, 6.5, 20),
    'm_hhBoosted': (r'm (HH) boosted [GeV]', 500, 3500, 30),
    'log_m_hhBoosted': ('log($m_{HH}^{boosted}$)',6.,9., 30),
    'pt_VBFj1Boosted': (r'$p_T (vbfj_1) [GeV]$', 20, 400, 19),
    'pt_VBFj2Boosted': (r'$p_T (vbfj_2) [GeV]$', 20, 200, 18),
    'log_pt_VBFj1Boosted': ('log($p_{T}^{VBFj1}$)',3.,9., 30),
    'log_pt_VBFj2Boosted':('log($p_{T}^{VBFj2}$)',3.,7., 20),
    'phi_VBFj1Boosted':('$\phi_{VBFj1}$',-3.2,3.2, 20),
    'E_VBFj1Boosted':  ('$E_{VBFj1}$ [GeV]',0.,2400., 20),
    'phi_VBFj2Boosted':('$\phi_{VBFj2}$',-3.2,3.2, 20),
    'E_VBFj2Boosted': ('$E_{VBFj2}$ [GeV]',0.,2400., 20),
    'eta_VBFj1Boosted': (r'$\eta (j_1)$', -5, 5, 20),
    'eta_VBFj2Boosted': (r'$\eta (j_2)$', -5, 5, 20),
    'delta_eta_VBFjjBoosted': (r'$\Delta\eta_{jj}$', 0, 10, 20),
    'm_VBFjjBoosted': (r'$m (j_1j_2) [GeV]$', 0, 4000, 20),
#    'XHHBoosted': (r'X (HH)', 0, 10, 100),
    'totalWeightBoosted': ("Event weight", 0, 10, 20),
    'HbbWP_h1Boosted': (r'$X_{bb} 1$', 20, 120, 10),
    'HbbWP_h2Boosted': (r'$X_{bb} 2$', 20, 120, 10),
#    'rel_pt_h1Boosted': (r'$p_T/m (H_1)$', 0, 30),
#    'rel_pt_h2Boosted': (r'$p_T/m (H_2)$', 0, 20),
    'log_pt_h1Boosted': (r'log($p_T (H_1)$)', 6, 7, 20),
    'log_pt_h2Boosted': (r'log($p_T (H_2)$)', 5, 7, 40),
    'log_pt_hhBoosted': (r'log($p_T (HH)$)', 4, 9, 20),
    'pt_hhBoosted': (r'$p_T (HH)$ [GeV]', 40, 400, 18),
    'One_Hot_encoder_Bit_2': (r'One_Hot_encoder_Bit_2',-1,2, 6),
    'One_Hot_encoder_Bit_1': (r'One_Hot_encoder_Bit_1',-1,2, 6),
}


def plot_1D(df, plot_variables, plot_options, extra_text='', output=None, batch=False, legend_loc=(0.6, 0.68), run_chi2=False, draw_legend=True):

    analysis_label_options = {
        'status': 'Internal',
        'loc': (0.05, 0.95),
        'energy': '13 TeV',
        'lumi': r'139 fb$^{-1}$',
        'fontsize': 28,
        'extra_text': f'VBF 4b {extra_text}',
    }
    styles = {
        'legend':{
            'loc': legend_loc,
            'fontsize': 22
        },
        'axis':{
            'tick_bothsides': True,
            'major_length': 12,
            'labelsize': 25,
        },
        'xlabel': {
            'fontsize': 30,
        },
        'ylabel': {
            'fontsize': 30,
        },
    }

    plotter = myScoreDistributionPlot(df, plot_options=plot_options, 
                                    analysis_label_options=analysis_label_options, styles=styles)
    plotter.run_chi2(run_chi2)
    for var in plot_variables:
        plotter.draw(column_name=var, xlabel=VARIABLES_1D[var][0],
                          xmin=VARIABLES_1D[var][1], xmax=VARIABLES_1D[var][2], 
                          nbins=VARIABLES_1D[var][3] if len(VARIABLES_1D[var]) == 4 else 40,
                          draw_legend=draw_legend)
        if var in ['delta_eta_VBFjjBoosted', 'XHHBoosted']:
            plotter.ax.set_ylim(0.00001, plotter.ax.get_ylim()[1] * 100)
            plotter.ax.set_yscale('log')
        else:
            plotter.ax.set_ylim(0, plotter.ax.get_ylim()[1] * 1.5)
        if output:
            makedirs(f'{output}/figs/', exist_ok=True)
            plt.savefig(f'{output}/figs/dist_1D_{extra_text.replace(" ", "_")}_{var}.pdf')
        if not batch:
            plt.show()
    return plotter

def plot_1D_with_errorbar(df, label_map, plot_variables, comparison_options=None, extra_text='', weight="totalWeightBoosted", output=None, batch=False, legend_loc=(0.6, 0.68)):

    analysis_label_options = {
        'loc': (0.05, 0.95),
        "energy": "13 TeV",
        "lumi": "139 fb$^{-1}$",
        "fontsize": 30,
        "extra_text": extra_text,
    }
    styles = {
        'legend':{
            'loc': legend_loc,
            'fontsize': 22
        },
        'errorbar': {
            "alpha": 0.7,
        }
    }

    for var in plot_variables:
        plotter = myPdfDistributionPlot.from_dataframes(df, observable=var,
                                                      weight=weight, range=(VARIABLES_1D[var][1], VARIABLES_1D[var][2]), 
                                                      bins=VARIABLES_1D[var][3] if len(VARIABLES_1D[var]) == 4 else 20, normalize=True,
                                                      error_method="auto",
                                                      label_map=label_map,
                                                      analysis_label_options=analysis_label_options, styles=styles)
        
        plotter.draw(xlabel=VARIABLES_1D[var][0], ylabel="Fraction of Events", ypad=0.25, 
                comparison_options=comparison_options)
        if output:
            makedirs(f'{output}/figs/', exist_ok=True)
            plt.savefig(f'{output}/figs/dist_1Derr_{extra_text.replace(" ", "_")}_{var}.pdf')
        if not batch:
            plt.show()
    return plotter

def plot_2D(plot_df, plot_variables, plot_options, output, weight_name='totalWeightBoosted', plot_SR_def=[], batch=False, CR_boundary= ''):
    
    analysis_label_options = {
        'status': 'Internal',
        'loc': (0.05, 0.95),
        'energy': '13 TeV',
        'lumi': r'139 fb$^{-1}$',
        'fontsize': 28,
    }
    styles = {
        'legend':{
            'loc': (0.6, 0.68),
            'fontsize': 22
        },
        'axis':{
            'tick_bothsides': True,
            'major_length': 12,
            'labelsize': 25,
        },
        'xlabel': {
            'fontsize': 30,
        },
        'ylabel': {
            'fontsize': 30,
        },
        'z-axis': {
            'major_length': 5,
            'major_width': 2, 'minor_width': 0,
            'spine_width': 0,
            'labelsize': 23, 'offsetlabelsize': 20,
            'tick_bothsides': False,
        },
        'zlabel': {
            'fontsize': 30,
        }
    }
    xscore_name, xlabel, xmin, xmax, xbins = plot_variables[0]
    yscore_name, ylabel, ymin, ymax, ybins = plot_variables[1]
    show_max = True if plot_SR_def else False

    if plot_SR_def:
        def Xhh(m1, m2, center_x=124, center_y=117, res=0.1):
            return np.sqrt(
                ((m1 - center_x) / (res * m1)) ** 2 + ((m2 - center_y) / (res * m2)) ** 2
            )
        def SR_bound1(m1, m2, fm1, fm2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (fm1 / m1 )) ** 2 + ((m2 - center_y) / (fm2 / m2)) ** 2
            )
        def CR_bound1(m1, m2, center_x=124, center_y=117):
            return np.sqrt(
                (m1 - center_x) ** 2 + (m2 - center_y) ** 2
            )
        def CR_bound2(m1, m2, fm1, fm2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (0.1* m1 )) ** 2 + ((m2 - center_y) / (0.1* m2)) ** 2
            ) + np.sqrt( (m1 - 120)**2 + (m2 - 110)**2 )
        def CR_bound3(m1, m2, fm1, fm2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (0.1* np.log(m1) )) ** 2 + ((m2 - center_y) / (0.1* np.log(m2))) ** 2
            ) + np.sqrt( (m1 - 120)**2 + (m2 - 110)**2 )
        def CR_bound3_v2(m1, m2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (0.1* np.log(m1) )) ** 2 + ((m2 - center_y) / (0.1* np.log(m2))) ** 2
            )
        def SR_bound2(m1, m2, fm1, fm2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (2*fm1 / (m1+m2) )) ** 2 + ((m2 - center_y) / (fm2 / m2)) ** 2
            )
        def SR_bound3(m1, m2, fm1, fm2, center_x=124, center_y=117):
            return np.sqrt(
                ((m1 - center_x) / (fm1 / m1 )) ** 2 + ((m2 - center_y) / (2*fm2 / (m1+m2))) ** 2
            )

        x = np.linspace(xmin, xmax, 1000)
        y = np.linspace(ymin, ymax, 1000)
        X, Y  = np.meshgrid(x,y)
        
    for key in plot_options:
        analysis_label_options['extra_text'] = f'VBF 4b, 2J2j, ' + plot_options[key].get('extra_text', key)
        ax = my_score_distribution_plot_2D(plot_df, hist_options={key: plot_options[key]}, xscore_name=xscore_name, yscore_name=yscore_name, \
                               weight_name=weight_name, xlabel=xlabel, ylabel=ylabel, zlabel='Events / bin', \
                               xmin=xmin, xmax=xmax, xbins=xbins, ymin=ymin, ymax=ymax, ybins=ybins, \
                              plot_styles = styles, analysis_label_options=analysis_label_options, show_text=False, show_max=show_max)
    
        if 0 in plot_SR_def:
            # SR variable (1.6 is the nominal cutoff)
            ax.contour(X, Y, Xhh(X,Y), [0, 1.6], colors='k', linewidths=2)
        if 1 in plot_SR_def:
            cp = ax.contour(X, Y, SR_bound1(X,Y,fm1=1500, fm2=1900), [1.6], colors='#f2385a', linewidths=2) # pink test1
            ax.clabel(cp, inline=True)
        if -1 in plot_SR_def:
            #cp = ax.contour(X, Y, CR_bound1(X,Y,center_x=300, center_y=250), [270], colors='#4ad9d9', linewidths=2) # pink test1
            #cp = ax.contour(X, Y, CR_bound2(X,Y,fm1=1500, fm2=1900), [40, 60], colors='#fdc536', linewidths=2) # pink test1
            #cp = ax.contour(X, Y, CR_bound3(X,Y,fm1=1500, fm2=1900), [140, 220], colors='#4ad9d9', linewidths=2) # pink test1
            cp = ax.contour(X, Y, CR_bound3_v2(X,Y), [100, 170], colors='#4ad9d9', linewidths=2) # pink test1
            ax.clabel(cp, inline=True)
        if -2 in plot_SR_def:
            if CR_boundary:
                x = np.linspace(0, 100,  1000)
                y = CR_boundary - x
                plt.plot(x,y)
            cp = ax.contour(X, Y, CR_bound3_v2(X,Y), [100], colors='#4ad9d9', linewidths=2) # pink test1
            ax.clabel(cp, inline=True)
        if 2 in plot_SR_def:
            cp = ax.contour(X, Y, SR_bound2(X,Y,fm1=1500, fm2=1800), [1.6], colors='#fdc536', linewidths=2) # yellow test2
            ax.clabel(cp, inline=True)
        if 3 in plot_SR_def:
            cp = ax.contour(X, Y, SR_bound3(X,Y,fm1=1200, fm2=1900), [2.0], colors='#4ad9d9', linewidths=2) # blue ZA0&ZAall
            ax.clabel(cp, inline=True)
        makedirs(f'{output}/figs/', exist_ok=True)
        plt.savefig(f'{output}/figs/massplane_2D_{key.replace(" ", "_")}.pdf')
        print(f'Save to {output}/figs/massplane_2D_{key.replace(" ", "_")}.pdf')
        if not batch:
            plt.show()


def my_score_distribution_plot_2D(dfs:Dict[str, pd.DataFrame], hist_options:Dict[str, Dict], 
                            data_options:Optional[Dict[str, Dict]]=None,
                            xbins:int=25, xmin:float=0, xmax:float=1, ybins:int=25, ymin:float=0,
                            ymax:float=1, xscore_name:str='score', yscore_name:str='score', weight_name:str='weight',
                            xlabel:str='NN Score 1', ylabel:str='NN Score 2', zlabel:str='Fraction of Events / bin',
                            coordinates:Optional[List]=None, plot_styles:Optional[Dict]=None,
                            analysis_label_options:Optional[Dict]=None, show_text=False, show_max=False, density=False, plot_contour=True):
    styles = parse_styles(plot_styles)
    ax = single_frame(styles=styles, analysis_label_options=analysis_label_options)
    key = list(hist_options.keys())[0]
    samples = hist_options[key]['samples']
    hist_style = hist_options[key].get('style_2D', {})
    logz = hist_style.pop('logz', False)
    if logz:
        log_scale = colors.LogNorm()
    else:
        log_scale = colors.Normalize()
    combined_df = pd.concat([dfs[sample] for sample in samples], ignore_index = True)
    norm_weights = combined_df[weight_name]
    if density:
        norm_weights /= norm_weights.sum()
    hist, x, y, image = ax.hist2d(x=combined_df[xscore_name], y=combined_df[yscore_name], bins=[xbins, ybins],
                        range=[[xmin, xmax], [ymin, ymax]], weights=norm_weights, **hist_style, 
                        norm=log_scale, zorder=-5)

    #if plot_contour:
    #    def get_grid(X_range, Y_range, num_grid_points):
    #        X_points = np.linspace(*X_range, num_grid_points)
    #        Y_points = np.linspace(*Y_range, num_grid_points)
    #        X_grid, Y_grid = np.meshgrid(X_points, Y_points)
    #        return X_grid, Y_grid

    #    x_centre = (x[:-1] + x[1:]) / 2
    #    y_centre = (y[:-1] + y[1:]) / 2
    #    hist_x, hist_y = np.meshgrid(x_centre, y_centre)
    #    hist_x = hist_x.reshape(-1)
    #    hist_y = hist_y.reshape(-1)
    #    hist_hist = hist.reshape(-1)
    #    X, Y = get_grid(X_range=(x_centre.min(), x_centre.max()), Y_range=(y_centre.min(), y_centre.max()), num_grid_points=100)
    #    Z = interpolate.griddata(np.stack((hist_x, hist_y), axis=1), hist_hist, (X, Y), method='cubic')
    #    if logz:
    #        levels = np.logspace(hist_hist.min(), hist_hist.max(), 5)
    #    else:
    #        levels = np.linspace(hist_hist.min(), hist_hist.max(), 5)
    #    print(levels)
    #    #ax.pcolormesh(X, Y, Z, shading='auto')
    #    #cp = ax.contour(Y, X, Z, levels=levels, linewidths=3)
    #    cp = ax.contour(X, Y, Z, levels=levels, linewidths=3)
    #    #ax.clabel(cp, inline=True)

    if show_max:
        index = unravel_index(hist.argmax(), hist.shape)
        ax.scatter(x[index[0]], y[index[1]], c='r')
        ax.text(x[index[0]], y[index[1]]+5, f'({x[index[0]]:.1f}, {y[index[1]]:.1f})', color="k", ha="center", va="center", fontweight="bold")
    if show_text:
        for i in range(len(y)-1):
            for j in range(len(x)-1):
                ax.text((x[j]+x[j+1])/2,(y[i]+y[i+1])/2, h.T[i,j], color="r", ha="center", va="center", fontweight="bold")

    if coordinates is not None:
        for coordinate in coordinates:
            ax.plot([i[0] for i in coordinate], [i[1] for i in coordinate], 'r--')
    ax.xaxis.set_major_locator(MaxNLocator(steps=[10]))
    ax.yaxis.set_major_locator(MaxNLocator(steps=[10]))
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    ax.set_xlabel(xlabel, **styles['xlabel'])
    ax.set_ylabel(ylabel, **styles['ylabel'])
    cbar = plt.colorbar(image, ax=ax, pad=0.02)
    format_axis_ticks(cbar.ax, x_axis=False, y_axis=True, ytick_styles=styles['ytick'], **styles['z-axis'])
    cbar.ax.yaxis.set_label_position("right")
    cbar.ax.yaxis.tick_right()
    cbar.ax.set_ylabel(zlabel, **styles['zlabel'])
    return ax


def yields_table(df, samples_and_weights, header=''):
    '''
    !: a dashed line for separation
    +: use + line as initial value for the rest in a block defined between any two sign of ! and +
    '''
    yields_result = {}
    if header:
        print(header)
    norm_yields = None
    for sample_and_weight in samples_and_weights:
        sample, weight = sample_and_weight
        if sample in ['+', '!']:
            print('-'*90)
            norm_yields = sum([df[s][w].sum() for s,w in weight]) if weight is not None else None
            continue
        if weight in df[sample]:
            title = sample
            if weight != 'totalWeightBoosted':
                title += ',' + weight.split('_S')[-1]

            if norm_yields is not None:
                percent = float(df[sample][weight].sum() / norm_yields)
                percent_str = ' ({:+.2f}%)'.format(percent*100)
            else:
                percent = None
                percent_str = ''

            print('{:35s} | {:8s} | {:10s} | {:.3f}'.format(title, str(df[sample].shape[0]), 'weighted', df[sample][weight].sum()) + percent_str)
            yields_result[title] = {
                'raw': int(df[sample].shape[0]),
                'weighted': float(df[sample][weight].sum()),
                'percentratio': percent,
            }
        else:
            print('{:35s} | {:8s} | {:10s} | {:.3f} ({:+.2f}%)'.format(title, str(df[sample].shape[0]), 'weighted', -1, -1))
    print('-'*90)
    print()
    return yields_result


def read_limits(file_name, kvv_list:list, blind:bool=True):
    from quickstats.maths.numerics import str_decode_value
    limits = {}
    for kvv in kvv_list:
        if blind:
            trexlimit = uproot.open(f'{file_name}{kvv}/Limits/asymptotics/myLimit_BLIND_CL95.root')['stats']
        else:
            trexlimit = uproot.open(f'{file_name}{kvv}/Limits/asymptotics/myLimit_CL95.root')['stats']

        kvv_value = str_decode_value(kvv)
        scale = 1
        if kvv in ['1']:
            scale = 1000
        limits[kvv_value] = {}
        limits[kvv_value]['k2v'] = kvv_value
        limits[kvv_value]['obs'] = trexlimit['obs_upperlimit'].array()[0] * scale
        limits[kvv_value]['0'] = trexlimit['exp_upperlimit'].array()[0] * scale
        limits[kvv_value]['-2'] = trexlimit['exp_upperlimit_minus2'].array()[0] * scale
        limits[kvv_value]['-1'] = trexlimit['exp_upperlimit_minus1'].array()[0] * scale
        limits[kvv_value]['2'] = trexlimit['exp_upperlimit_plus2'].array()[0] * scale
        limits[kvv_value]['1'] = trexlimit['exp_upperlimit_plus1'].array()[0] * scale
    df = pd.DataFrame(limits).T
    df.reset_index(drop=True, inplace=True)
    df.to_json(f'{file_name}.json', indent=2)
    return df

def plot_k2v(kvv_list, file_name, extra_text):
    analysis_label_options = {
            'status': 'int',
            'energy': '13 TeV',
            'lumi': '139 fb$^{-1}$',
            'fontsize': 30,
            'loc': (0.05, 0.95),
            'extra_text': extra_text,
    }

    styles = {
            'legend':{
                'loc': (0.58, 0.67),
                'fontsize': 17
                }
    }

    from cores.limit_scan_plot import LimitScanPlot
    df = read_limits(file_name, kvv_list)
    plotter = LimitScanPlot(f"{file_name}.json", "k2v", analysis_label_options=analysis_label_options, styles = styles)
    ax = plotter.draw(vbf_only = True, ylim=(1, 10000))
    plt.savefig(f'{file_name}.pdf')
    return df, ax
