from os import makedirs, path
from pdb import set_trace

class trexConfig:
    def __init__(self, config:dict={}):
        self.config = config

    def head(self, kvv, all_sig_samples):
        fitType = self.config.get('fitType', 'SPLUSB')
        
        POIAsimov = 1
        all_sig_samples = ', '.join(all_sig_samples)
        input_file = path.join(self.config['input_dir'], 'bkgreweight', self.config['input_sub_dir'], self.config['input_hist'])
        job_name = self.config['job_name']
        fit_output = path.join(self.config['input_dir'], 'fit', self.config['input_sub_dir'], self.config['fit_output'])

        blind = self.config['blind']
        return f'''Job: "{job_name}_kvv{kvv}"
  OutputDir: {fit_output}
  BlindSRs: {blind}
  ReadFrom: HIST
  DebugLevel: 0
  POI: "SigXsecOverSM"
  HistoPath: {'/'.join(input_file.split('/')[:-1])}
  HistoFile: {input_file.split('/')[-1].split('.root')[0]}
  PlotOptions: OVERSIG,NOXERR,YIELDS,CHI2
  MCstatThreshold: 0.005
  SystPruningNorm: 0.00005
  SystPruningShape: 0.0001
  DoPieChartPlot: TRUE
  CmeLabel: "13 TeV"
  DoSummaryPlot: TRUE
  DoTables: TRUE
  SystCategoryTables: TRUE
  SystControlPlots: TRUE
  SystDataPlots: TRUE
  LegendNColumns: 1
  ImageFormat: "pdf"
  Lumi: 1
  LumiLabel: "139 fb^{{-1}}"
  GetChi2: TRUE
  UseATLASRoundingTxt: TRUE
  UseATLASRoundingTex: TRUE
  TableOptions: STANDALONE
  RankingPlot: Systs
  RankingMaxNP: 20
  RatioYmin: 0.80
  RatioYmax: 1.20
  RatioYminPostFit: 0.90
  RatioYmaxPostFit: 1.10
  SplitHistoFiles: TRUE
  CorrelationThreshold: 0.05
  MCstatConstraint: POISSON

Fit: {job_name}_kvv{kvv}
  NumCPU: 4
  POIAsimov: {POIAsimov}
  FitType: {fitType}
  FitRegion: CRSR
  FitBlind: {blind}

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: {blind}

NormFactor: "SigXsecOverSM"
  Max: 100
  Nominal: 1
  Min: -100
  Samples: {all_sig_samples}
  Title: "#it{{#mu}}_{{SIG}}"
    '''

    def sample(self, kvv, region, var):
        return f'''
Region: "{region}___{var}"
  VariableTitle: {var}
  Type: SIGNAL
  Label: "{region} {var}"
  LogScale: True
  Ymin: 1E-3
  Binning: AutoBin, TransfoD, 10, 10

Sample: "Sig_{region}___{var}"
  Title: "#it{{HH}}"
  TexTitle: HH
  FillColor: 862
  LineColor: 1
  HistoNames: {region.replace("CR", "SR")}kvv{kvv}___{var}
  Type: SIGNAL
  MCstatScale: 1
  Regions: {region}___{var}

Sample: "Bkg_{region}___{var}"
  Title: "{region}"
  TexTitle: "{region}"
  FillColor: 5
  LineColor: 1
  HistoNames: {region.replace("0F", "1F")}data___{var}
  Type: BACKGROUND
  UseMCstat: TRUE
  SeparateGammas: True
  Regions: {region}___{var}
''', f'Sig_{region}___{var}'
    
    def data_sample(self, region, var):
        return f'''
Sample: "Data_{region}___{var}"
  Title: "Data"
  LineColor: 1
  HistoNames: {region.replace("0F", "1F")}data___{var}
  Type: DATA
  Regions: {region}___{var}
'''
    
    def write_config(self, kvv, label):# kvv_list = ['0', '0p5', '1', '1p5', '2', '3']
        contents = []
        all_sig_samples = []
    
        regions = self.config['regions']
        variables = self.config['variables']
        for region in regions:
            for var in variables:
                sample, sig_sample = self.sample(kvv, region, var)
                contents.append(sample)
                all_sig_samples.append(sig_sample)
                sample = self.data_sample(region, var)
                contents.append(sample)

        config_name = f'T{label}_kvv{kvv}.config'
        contents.insert(0, self.head(kvv, all_sig_samples))

        config_name = path.join(self.config['input_dir'], 'fit', self.config['input_sub_dir'], 'config', config_name)
        makedirs(path.dirname(config_name), exist_ok=True)
        with open(config_name, 'w') as f:
            for content in contents:
                print(content, file=f)
    
        print(config_name)
        return config_name
