from pdb import set_trace
from quickstats.plots.color_schemes import QUICKSTATS_PALETTES
import copy
color = QUICKSTATS_PALETTES["default"]
import sys, os
from os import path, environ, makedirs, stat
from cores.cut_flow import *
from cores.plot_utils import *
from cores.bkg_utls import *
from cores.ntuple_utils import *
from cores.trex_config import *

class analysis:
    def __init__(self, rw_strategy=1, CR_boundary=50, framework='', output='',alter=False):
        '''
        rw_strategy: 1 - first learner
                     2 - second learner
                     3 - first learner with loose SR
                     4 - first learner with HP, MP, Lo SR
                     5 - learner using 1F and 0
        '''
        self.rw_strategy = rw_strategy
        sys.path.append(framework)
        self.output = output
        self.CR_boundary = CR_boundary
        self.alter=alter
    def get_bkgmodel_df(self, cache_path):
        all_sample_dfs = load_ntuple(cache_path)
        all_sample_dfs = construct_variables(all_sample_dfs)
        self.cutflowobj = CutFlowObj(all_sample_dfs, self.rw_strategy, self.CR_boundary, self.alter)
        for whichcut in ['']:
            self.cutflowobj.df_apply_cut(whichcut)    
        self.bkgmodel_df = self.cutflowobj.SR_df
        self.bkgmodel_df = construct_variable_for_BDT(self.bkgmodel_df)
        self.cutflowobj.print_cutflow(samples = ['data','kvv0'])

    def train_study(self, precuts=[''], batch=False, cache=True, nseeds:int=10, iseed:int=None):
        '''
        precuts: which precuts to apply on training
        iseed: 0...nseeds, for parallelisation; None to run all seeds
        '''
        self.output_sub_dir = []
        self.training_variables = {
            #'pt_eta_J': ['log_pt_h1Boosted', 'log_pt_h2Boosted', 'eta_h1Boosted', 'eta_h2Boosted', 'delta_ptHH_boosted', 'delta_etaHH_boosted'，'One_Hot_encoder_Bit_1'，'One_Hot_encoder_Bit_2'],
            #'pt_eta_J_mHH': ['log_pt_h1Boosted', 'log_pt_h2Boosted', 'eta_h1Boosted', 'eta_h2Boosted', 'log_delta_ptHH_boosted', 'delta_etaHH_boosted', 'm_hhBoosted','One_Hot_encoder_Bit_1','One_Hot_encoder_Bit_2'],
            'all': ['log_pt_h1Boosted','eta_h1Boosted','log_m_h1Boosted','log_pt_h2Boosted','eta_h2Boosted','log_m_h2Boosted','log_pt_hhBoosted','eta_hhBoosted','log_m_hhBoosted','log_pt_VBFj1Boosted','eta_VBFj1Boosted','log_pt_VBFj2Boosted','eta_VBFj2Boosted','log_delta_ptHH_boosted','One_Hot_encoder_Bit_1','One_Hot_encoder_Bit_2']

        }
        self.reweighters = {}
        for which_precut in precuts:
            for varset in self.training_variables.keys():
                print('run', which_precut, varset)
                bkgRW_config = {
                    'data_map': copy.deepcopy(self.bkgmodel_df),
                    'train_which_precut': which_precut,
                    'apply_which_precut': '',
                    'cache': cache,
                    'nseeds': nseeds,
                    'iseed': iseed,
                    #'bootstrap': True,
                   # 'bootstrap_reserve_norm': True,
                    'bootstrap_reserve_norm': False,
                    'epochs': 200,
                    'patience': 10,
                    'train_columns': self.training_variables[varset],
                    'output_dir': self.output,
                    'rw_strategy': self.rw_strategy,
                }
                if self.rw_strategy == 1:
                    bkgRW_config['train_target'] = 'SR2F'
                    bkgRW_config['train_origin'] = 'CR2F'
                    bkgRW_config['validation_target'] = 'SR1F'
                    bkgRW_config['validation_origin'] = 'CR1F'
                    bkgRW_config['apply_target'] = ['CR0F']
                    bkgRW_config['apply_origin'] = ['CR0F']
                    if self.alter: # alternative training
                        bkgRW_config['train_target'] = 'SR1F'
                        bkgRW_config['train_origin'] = 'CR1F'
                        bkgRW_config['validation_target'] = 'SR2F'
                        bkgRW_config['validation_origin'] = 'CR2F'
                        bkgRW_config['apply_target'] = ['CR0F']
                        bkgRW_config['apply_origin'] = ['CR0F']
                elif self.rw_strategy == 2:
                    bkgRW_config['train_target'] = 'CR0F'
                    bkgRW_config['train_origin'] = 'CR2F'
                    bkgRW_config['validation_target'] = 'VR0F'
                    bkgRW_config['validation_origin'] = 'VR2F'
                    bkgRW_config['apply_target'] = ['SR2F']
                    bkgRW_config['apply_origin'] = ['SR2F']
                    if self.alter: # alternative training
                        bkgRW_config['train_target'] = 'VR0F'
                        bkgRW_config['train_origin'] = 'VR2F'
                        bkgRW_config['validation_target'] = 'CR0F'
                        bkgRW_config['validation_origin'] = 'CR2F'
                        bkgRW_config['apply_target'] = ['SR2F']
                        bkgRW_config['apply_origin'] = ['SR2F']
                elif self.rw_strategy == 3:
                    bkgRW_config['train_target'] = 'SR2FLo'
                    bkgRW_config['train_origin'] = 'CR2FLo'
                    bkgRW_config['validation_target'] = 'SR1FLo'
                    bkgRW_config['validation_origin'] = 'CR1FLo'
                    bkgRW_config['apply_target'] = ['CR0FLo', 'CR0FTi']
                    bkgRW_config['apply_origin'] = ['CR0FLo', 'CR0FTi']
                    if self.alter:
                        bkgRW_config['train_target'] = 'SR1FLo'
                        bkgRW_config['train_origin'] = 'CR1FLo'
                        bkgRW_config['validation_target'] = 'SR2FLo'
                        bkgRW_config['validation_origin'] = 'CR2FLo'
                        bkgRW_config['apply_target'] = ['CR0FLo', 'CR0FTi']
                        bkgRW_config['apply_origin'] = ['CR0FLo', 'CR0FTi']
                elif self.rw_strategy == 4:
                    bkgRW_config['train_target'] = 'SR2FLo'
                    bkgRW_config['train_origin'] = 'CR2FLo'
                    bkgRW_config['validation_target'] = 'SR1FLo'
                    bkgRW_config['validation_origin'] = 'CR1FLo'
                    bkgRW_config['apply_target'] = ['CR0FLo', 'CR0FHP', 'CR0FMP']
                    bkgRW_config['apply_origin'] = ['CR0FLo', 'CR0FHP', 'CR0FMP']
                    if self.alter:
                        bkgRW_config['train_target'] = 'SR1FLo'
                        bkgRW_config['train_origin'] = 'CR1FLo'
                        bkgRW_config['validation_target'] = 'SR2FLo'
                        bkgRW_config['validation_origin'] = 'CR2FLo'
                        bkgRW_config['apply_target'] = ['CR0FLo', 'CR0FHP', 'CR0FMP']
                        bkgRW_config['apply_origin'] = ['CR0FLo', 'CR0FHP', 'CR0FMP']
                elif ((self.rw_strategy == 5) & (self.alter ==False)):
                    bkgRW_config['train_target'] = 'CR0FLo'
                    bkgRW_config['train_origin'] = 'CR1FLo'
                    bkgRW_config['validation_target'] = 'CR0FLo'
                    bkgRW_config['validation_origin'] = 'CR1FLo'
                    bkgRW_config['apply_target'] = ['CR0FLo']
                    bkgRW_config['apply_origin'] = ['CR1FLo']  
                elif ((self.rw_strategy == 5) & (self.alter==True)):
                    bkgRW_config['train_target'] = 'CR0F'
                    bkgRW_config['train_origin'] = 'CR1F'
                    bkgRW_config['validation_target'] = 'SR2F'
                    bkgRW_config['validation_origin'] = 'CR2F'
                    bkgRW_config['apply_target'] = ['CR0F']
                    bkgRW_config['apply_origin'] = ['CR1F']
                bkgRW_config['output_sub_dir'] = f'bkgreweight/{bkgRW_config["validation_origin"]}_{which_precut}_{varset}_stg{self.rw_strategy}'
                reweighter = bkgReweighter(bkgRW_config)
                reweighter.train_bkg_reweighter()
                if not batch:
                    reweighter.apply_to_df()
                    reweighter.reweighted_yields()

                    additional_varibles_to_plot = ['m_hhBoosted', 'm_h1Boosted', 'm_h2Boosted', 'pt_hhBoosted', 'pt_h1Boosted', 'pt_h2Boosted', 'pt_VBFj1Boosted', 'pt_VBFj2Boosted']
                    #reweighter.reweighted_plots(additional_varibles_to_plot, which_dataset='train')
                    #reweighter.reweighted_plots(additional_varibles_to_plot)
                    reweighter.reweighted_plots_witherror(additional_varibles_to_plot, which_dataset='train')
                    reweighter.reweighted_plots_witherror(additional_varibles_to_plot)

                    reweighter.reweighted_plots(additional_varibles_to_plot)
                    reweighter.save(fname=f'all_results-{iseed}-', chi2=not batch)
                self.reweighters[which_precut+varset] = reweighter
                self.output_sub_dir.append(os.path.basename(bkgRW_config['output_sub_dir']))
                #reweighter.plot_loss()
        return self.reweighters

    def apply_weights(self):
        if self.rw_strategy == 1:
            process = ['VR0Fdata', 'CR0Fdata', 'SR1Fdata', 'VR1Fdata', 'CR1Fdata', 'SR2Fdata', 'VR2Fdata', 'CR2Fdata']
        elif self.rw_strategy == 2:
            process = ['SR2Fdata', 'VR2Fdata']
        elif self.rw_strategy == 3:
            process = ['VR0FTidata', 'CR0FTidata', 'VR0FLodata', 'CR0FLodata', 'SR1FLodata', 'VR1FLodata', 'CR1FLodata', 'SR2FLodata', 'VR2FLodata', 'CR2FLodata']
        elif self.rw_strategy == 4:
            process = ['VR0FHPdata', 'CR0FHPdata', 'VR0FMPdata', 'CR0FMPdata', 'VR0FLodata', 'CR0FLodata', 'SR1FLodata', 'VR1FLodata', 'CR1FLodata', 'SR2FLodata', 'VR2FLodata', 'CR2FLodata']
        elif ((self.rw_strategy == 5) & (self.alter==False)):
            process =['SR1FLodata','CR1FLodata','VR1FLodata']
        elif ((self.rw_strategy == 5) & (self.alter==True)):
             process =['SR1Fdata','CR1Fdata','VR1Fdata']
        else:
            assert(0), f'Unknown strategy {self.rw_strategy}'
            
        for reweight in self.reweighters:
            self.reweighters[reweight].apply_to_df(precuts=[''], processes=process, write_to_root=True)
            self.reweighters[reweight].save_template()

    def gen_trex_config(self, label=''):
        
        if self.rw_strategy == 1:
            regions = ['SR0F']
        elif self.rw_strategy == 2:
            regions = ['SR2F']
        elif self.rw_strategy == 3:
            regions = ['SR0FTi', 'SR0FLo']
        elif self.rw_strategy == 4:
            regions = ['SR0FHP', 'SR0FMP', 'SR0FLo']
        elif self.rw_strategy == 5:
            regions = ['SR0FLo']
        else:
            assert(0), f'Unknown strategy {self.rw_strategy}'
        for reweighting in self.output_sub_dir:
            fit_config = {
                'input_dir': self.output,
                'input_sub_dir': reweighting,
                'input_hist': 'Apply/Hist/hist.root',
                'fitType': 'SPLUSB',
                'blind': True,
                'job_name': f'T{label}',
                'variables': ['m_hhBoosted'],
                'regions': regions,
                'fit_output': 'fit_output',
            }
            trex = trexConfig(fit_config)
            if self.rw_strategy != 5:
                kvv_list = ['0', '0p5', '1', '1p5', '2', '3']
            elif self.rw_strategy == 5:
                 kvv_list = ['0', '0p5', '1', '1p5', '2', '3']
            for kvv in kvv_list:
                trex.write_config(kvv, label=label)

    def plot_k2v(self, label=''):
        kvv_list = ['0', '0p5', '1', '1p5', '2', '3']
        #kvv_list = ['0', '0p5']
        for reweighting in self.output_sub_dir:
            #os.makedirs(self.output+f'/fit/{reweighting}/fit_output/T{label}_kvv')
            file_name = self.output+f'/fit/{reweighting}/fit_output/T{label}_kvv'
            print(file_name)
            df, ax = plot_k2v(kvv_list, file_name, reweighting)
            plt.show()
            display(df)
    
    @staticmethod
    def check_first_learner(whichR = 'SR'):
        plot_1D_options = {
            f'data {whichR}0F': {
                'samples': [f'{whichR}0Fdata'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data {whichR}0F',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[0],
                }
            },
            f'data {whichR}1F': {
                'samples': [f'{whichR}1Fdata'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data {whichR}1F',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[1],
                }
            },
    
            f'data {whichR}2F': {
                'samples': [f'{whichR}2Fdata'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data {whichR}2F',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[2],
                }
            },
        }
    
        if whichR == 'SR':
            plot_1D_options.pop(f'data {whichR}0F')
    
        for key in self.bkgmodel_df:
            plot_1D(self.bkgmodel_df[key], ['pt_h1Boosted', 'pt_h2Boosted', 'eta_h1Boosted', 'eta_h2Boosted', 'm_hhBoosted', 'HbbWP_h1Boosted', 'HbbWP_h2Boosted'], plot_1D_options, key, self.output)
          
    
    def check_second_learner(self, whichF = '1F'):
        plot_1D_options = {
            f'data SR{whichF}': {
                'samples': [f'SR{whichF}data'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data SR{whichF}',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[0],
                }
            },
            f'data VR{whichF}': {
                'samples': [f'VR{whichF}data'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data VR{whichF}',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[1],
                }
            },
            f'data CR{whichF}': {
                'samples': [f'CR{whichF}data'],
                'type': 'norm',
                'weight_name': 'totalWeightBoosted',
                "style": {
                    'density': False,
                    'alpha': 0.7,
                    'histtype': 'step',
                    'label': f'Data CR{whichF}',
                    'linewidth': 4,
                    'linestyle': '-',
                    'color': color[2],
                }
            },
        }
    
        for key in self.bkgmodel_df:
            plot_1D(self.bkgmodel_df[key], ['pt_h1Boosted', 'pt_h2Boosted', 'eta_h1Boosted', 'eta_h2Boosted', 'm_hhBoosted', 'HbbWP_h1Boosted', 'HbbWP_h2Boosted'], plot_1D_options, key, self.output)



    def check_cut_flow(self):
        if self.rw_strategy in [1, 2]:
            samples_and_weights = [
                # data
                ('!', None),
                ('NonSR0Fdata', 'totalWeightBoosted'),
                ('VR0Fdata', 'totalWeightBoosted'),
                ('CR0Fdata', 'totalWeightBoosted'),
                
                ('+', [('SR1Fdata', 'totalWeightBoosted'), ('NonSR1Fdata', 'totalWeightBoosted')]),
                ('SR1Fdata', 'totalWeightBoosted'),
                ('NonSR1Fdata', 'totalWeightBoosted'),
                ('VR1Fdata', 'totalWeightBoosted'),
                ('CR1Fdata', 'totalWeightBoosted'),
                ('+', [('SR2Fdata', 'totalWeightBoosted'), ('NonSR2Fdata', 'totalWeightBoosted')]),
                ('SR2Fdata', 'totalWeightBoosted'),
                ('NonSR2Fdata', 'totalWeightBoosted'),
                ('VR2Fdata', 'totalWeightBoosted'),
                ('CR2Fdata', 'totalWeightBoosted'),
            
                # kvv0
                ('+', [('SR0Fkvv0', 'totalWeightBoosted'), ('NonSR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0Fkvv0', 'totalWeightBoosted'),
                ('NonSR0Fkvv0', 'totalWeightBoosted'),
                ('VR0Fkvv0', 'totalWeightBoosted'),
                ('CR0Fkvv0', 'totalWeightBoosted'),
            #     ('+', [('SR1Fkvv0', 'totalWeightBoosted'), ('NonSR1Fkvv0', 'totalWeightBoosted')]),
            #     ('SR1Fkvv0', 'totalWeightBoosted'),
            #     ('NonSR1Fkvv0', 'totalWeightBoosted'),
            #     ('VR1Fkvv0', 'totalWeightBoosted'),
            #     ('CR1Fkvv0', 'totalWeightBoosted'),
            #     ('+', [('SR2Fkvv0', 'totalWeightBoosted'), ('NonSR2Fkvv0', 'totalWeightBoosted')]),
            #     ('SR2Fkvv0', 'totalWeightBoosted'),
            #     ('NonSR2Fkvv0', 'totalWeightBoosted'),
            #     ('VR2Fkvv0', 'totalWeightBoosted'),
            #     ('CR2Fkvv0', 'totalWeightBoosted'),
                ('+', [('SR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0Fkvv0', 'totalWeightBoosted'),
                ('SR1Fkvv0', 'totalWeightBoosted'),
                ('SR2Fkvv0', 'totalWeightBoosted'),
            ]
        elif self.rw_strategy in [3]:
            samples_and_weights = [
                # data
                ('!', None),
                ('VR0FTidata', 'totalWeightBoosted'),
                ('VR0FLodata', 'totalWeightBoosted'),
                ('CR0FTidata', 'totalWeightBoosted'),
                ('CR0FLodata', 'totalWeightBoosted'),
                
                ('+', [('SR1Fdata', 'totalWeightBoosted'), ('NonSR1Fdata', 'totalWeightBoosted')]),
                ('SR1FLodata', 'totalWeightBoosted'),
                ('VR1FLodata', 'totalWeightBoosted'),
                ('CR1FLodata', 'totalWeightBoosted'),
                ('+', [('SR2Fdata', 'totalWeightBoosted'), ('NonSR2Fdata', 'totalWeightBoosted')]),
                ('SR2FLodata', 'totalWeightBoosted'),
                ('VR2FLodata', 'totalWeightBoosted'),
                ('CR2FLodata', 'totalWeightBoosted'),
            
                # kvv0
                ('+', [('SR0Fkvv0', 'totalWeightBoosted'), ('NonSR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0FTikvv0', 'totalWeightBoosted'),
                ('VR0FTikvv0', 'totalWeightBoosted'),
                ('CR0FTikvv0', 'totalWeightBoosted'),
                ('SR0FLokvv0', 'totalWeightBoosted'),
                ('VR0FLokvv0', 'totalWeightBoosted'),
                ('CR0FLokvv0', 'totalWeightBoosted'),
                ('+', [('SR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0FTikvv0', 'totalWeightBoosted'),
                ('SR0FLokvv0', 'totalWeightBoosted'),
                ('SR1FLokvv0', 'totalWeightBoosted'),
                ('SR2FLokvv0', 'totalWeightBoosted'),
            ]
        elif self.rw_strategy in [4]:
            samples_and_weights = [
                # data
                ('!', None),
                ('VR0FHPdata', 'totalWeightBoosted'),
                ('VR0FMPdata', 'totalWeightBoosted'),
                ('VR0FLodata', 'totalWeightBoosted'),
                ('CR0FHPdata', 'totalWeightBoosted'),
                ('CR0FMPdata', 'totalWeightBoosted'),
                ('CR0FLodata', 'totalWeightBoosted'),
                
                ('+', [('SR1Fdata', 'totalWeightBoosted'), ('NonSR1Fdata', 'totalWeightBoosted')]),
                ('SR1FLodata', 'totalWeightBoosted'),
                ('VR1FLodata', 'totalWeightBoosted'),
                ('CR1FLodata', 'totalWeightBoosted'),
                ('+', [('SR2Fdata', 'totalWeightBoosted'), ('NonSR2Fdata', 'totalWeightBoosted')]),
                ('SR2FLodata', 'totalWeightBoosted'),
                ('VR2FLodata', 'totalWeightBoosted'),
                ('CR2FLodata', 'totalWeightBoosted'),
            
                # kvv0
                ('+', [('SR0Fkvv0', 'totalWeightBoosted'), ('NonSR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0FHPkvv0', 'totalWeightBoosted'),
                ('VR0FHPkvv0', 'totalWeightBoosted'),
                ('CR0FHPkvv0', 'totalWeightBoosted'),
                ('SR0FMPkvv0', 'totalWeightBoosted'),
                ('VR0FMPkvv0', 'totalWeightBoosted'),
                ('CR0FMPkvv0', 'totalWeightBoosted'),
                ('SR0FLokvv0', 'totalWeightBoosted'),
                ('VR0FLokvv0', 'totalWeightBoosted'),
                ('CR0FLokvv0', 'totalWeightBoosted'),
                
                ('+', [('SR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0FHPkvv0', 'totalWeightBoosted'),
                ('SR0FMPkvv0', 'totalWeightBoosted'),
                ('SR0FLokvv0', 'totalWeightBoosted'),
                ('SR1FLokvv0', 'totalWeightBoosted'),
                ('SR2FLokvv0', 'totalWeightBoosted'),
            ]
        elif ((self.rw_strategy in [5]) & (self.alter == False)): 
             samples_and_weights = [
                 #data
                ('!', None),
                ('NonSR0FLodata', 'totalWeightBoosted'),
                ('VR0FLodata', 'totalWeightBoosted'),
                ('CR0FLodata', 'totalWeightBoosted'),
                 
                ('+', [('SR1FLodata', 'totalWeightBoosted'), ('NonSR1FLodata', 'totalWeightBoosted')]),
                ('SR1FLodata', 'totalWeightBoosted'),
                ('NonSR1FLodata', 'totalWeightBoosted'),
                ('VR1FLodata', 'totalWeightBoosted'),
                ('CR1FLodata', 'totalWeightBoosted'),
                 
                 # kvv0
                ('+', [('SR0FLokvv0', 'totalWeightBoosted'), ('NonSR0FLokvv0', 'totalWeightBoosted')]),
                ('SR0FLokvv0', 'totalWeightBoosted'),
                ('NonSR0FLokvv0', 'totalWeightBoosted'),
                ('VR0FLokvv0', 'totalWeightBoosted'),
                ('CR0FLokvv0', 'totalWeightBoosted'),
                 
                ('+', [('SR1FLokvv0', 'totalWeightBoosted'), ('NonSR1FLokvv0', 'totalWeightBoosted')]),
                ('SR1FLokvv0', 'totalWeightBoosted'),
                ('NonSR1FLokvv0', 'totalWeightBoosted'),
                ('VR1FLokvv0', 'totalWeightBoosted'),
                ('CR1FLokvv0', 'totalWeightBoosted'),
                 
                ('+', [('SR1Fkvv0', 'totalWeightBoosted'), ('NonSR1Fkvv0', 'totalWeightBoosted')]),
                ('SR1Fkvv0', 'totalWeightBoosted'),
                ('NonSR1Fkvv0', 'totalWeightBoosted'),
                ('VR1Fkvv0', 'totalWeightBoosted'),
                ('CR1Fkvv0', 'totalWeightBoosted'),
             ]
        elif ((self.rw_strategy in [5]) & (self.alter == True)): 
             samples_and_weights = [
                 #data
                ('!', None),
                ('NonSR0Fdata', 'totalWeightBoosted'),
                ('VR0Fdata', 'totalWeightBoosted'),
                ('CR0Fdata', 'totalWeightBoosted'),
                 
                ('+', [('SR1Fdata', 'totalWeightBoosted'), ('NonSR1Fdata', 'totalWeightBoosted')]),
                ('SR1Fdata', 'totalWeightBoosted'),
                ('NonSR1Fdata', 'totalWeightBoosted'),
                ('VR1Fdata', 'totalWeightBoosted'),
                ('CR1Fdata', 'totalWeightBoosted'),
                 
                 # kvv0
                ('+', [('SR0Fkvv0', 'totalWeightBoosted'), ('NonSR0Fkvv0', 'totalWeightBoosted')]),
                ('SR0Fkvv0', 'totalWeightBoosted'),
                ('NonSR0Fkvv0', 'totalWeightBoosted'),
                ('VR0Fkvv0', 'totalWeightBoosted'),
                ('CR0Fkvv0', 'totalWeightBoosted'),
                 
                ('+', [('SR1Fkvv0', 'totalWeightBoosted'), ('NonSR1Fkvv0', 'totalWeightBoosted')]),
                ('SR1Fkvv0', 'totalWeightBoosted'),
                ('NonSR1Fkvv0', 'totalWeightBoosted'),
                ('VR1Fkvv0', 'totalWeightBoosted'),
                ('CR1Fkvv0', 'totalWeightBoosted'),
                 
                ('+', [('SR1Fkvv0', 'totalWeightBoosted'), ('NonSR1Fkvv0', 'totalWeightBoosted')]),
                ('SR1Fkvv0', 'totalWeightBoosted'),
                ('NonSR1Fkvv0', 'totalWeightBoosted'),
                ('VR1Fkvv0', 'totalWeightBoosted'),
                ('CR1Fkvv0', 'totalWeightBoosted'),
             ]
        else:
            assert(0), f'Unknown strategy {self.rw_strategy}'
        
        for key in self.bkgmodel_df.keys():
            _ = yields_table(self.bkgmodel_df[key], header=key, samples_and_weights=samples_and_weights)

    #def evaluate(self, variableset=training_variables.keys(), variables=["m_hhBoosted"], bootstrap=True):
    #    evaluate_result = {}
    #    apply_which_precut = ''
    #    for which_precut in self.precut:
    #        for varset in variableset:
    #            output_sub_dir = f'bkgreweight/CR1F_{which_precut}_{varset}'
    #            if bkgRW_config['bootstrap']:
    #                output_sub_dir += '_withBootstrap'
    #            else:
    #                output_sub_dir += '_noBootstrap'
    #            result_path = path.join(output, output_sub_dir, f'all_results{nseeds}.json')
    #            with open(result_path) as fp:
    #                all_result = json.load(fp)
    #            evaluate_result[f'{which_precut}_{varset}'] = {
    #                f'norm CR2F': all_result['yields_table'][apply_which_precut]["CR2Fdata,median"]['ratio'],
    #                f'norm CR1F': all_result['yields_table'][apply_which_precut]["CR1Fdata,median"]['ratio'],
    #            }
    #            for iseed in range(nseeds):
    #                evaluate_result[f'{which_precut}_{varset}'][f'norm CR1F S{iseed}'] = all_result['yields_table'][apply_which_precut][f"CR1Fdata,{int(2022+iseed)}"]['ratio']
    #            for var in variables:
    #                evaluate_result[f'{which_precut}_{varset}'][f'chi2 {var} before'] = all_result['chi2'][apply_which_precut][var]["SR1Fdata vs CR1Fdata"]
    #                evaluate_result[f'{which_precut}_{varset}'][f'chi2 {var} after'] = all_result['chi2'][apply_which_precut][var]["SR1Fdata vs CR1Fdata (reweighted)"]
    #    return evaluate_result
    
    @staticmethod
    def compare_reweighter_plots(reweights, region = 'CR1FLodata', batch=False):
        df_map = {}
        plot_1D_options = {}
        for rwname, reweight in reweights.items():
            for which_precut in reweight.bkgmodel_df.keys():
                if not 'totalWeightBoosted_Smedian' in reweight.bkgmodel_df[which_precut][region]:
                    continue
                name = rwname + ' ' + which_precut
                if name == 'noVBFcutpt_eta_J noVBFcut': continue
                df_map[name] = reweight.bkgmodel_df[which_precut][region]
                print(name)
                plot_1D_options[name] = {
                    'samples': [name],
                    'type': 'norm',
                    'weight_name': 'totalWeightBoosted_Smedian',
                    "style": {
                        'density': False,
                        'alpha': 0.7,
                        'histtype': 'step',
                        'label': f'{name}',
                        'linewidth': 4,
                    }
                }
                plot_1D_options[name+"orig"] = {
                    'samples': [name],
                    'type': 'norm',
                    'weight_name': 'totalWeightBoosted',
                    "style": {
                        'density': False,
                        'alpha': 0.7,
                        'histtype': 'step',
                        'label': f'{name}orig',
                        'linewidth': 4,
                    }
                }
        additional_varibles = [
            'm_hhBoosted',
            'HbbWP_h1Boosted',
            'HbbWP_h2Boosted',
        ]
        plotter = plot_1D(df_map, plot_variables=reweight.train_columns + additional_varibles, plot_options=plot_1D_options, extra_text=region, legend_loc=(0.4, 0.95-0.07*len(plot_1D_options)), batch=batch)
        return df_map
