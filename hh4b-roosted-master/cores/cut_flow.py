import numpy as np
import pandas as pd
from quickstats.maths.numerics import pretty_value
from pdb import set_trace

class CutFlowObj:
    def __init__(self, dataframe, rw_strategy, CR_boundary,alter):
        self.all_sample_dfs = dataframe
        self.rw_strategy = rw_strategy
        self.alter= alter
        self.completevbfcut ={}
        self.SR_df = {}
        self.precut = {}
        self.largeRcut = {}
        self.vbfcut = {}
        self.Xbb0Fcut = {}
        self.Xbb2Fcut = {}
        self.Xbb1Fcut = {}
        self.Xbb0FHPcut = {}
        self.Xbb0FMPcut = {}
        self.Xbb0FTicut = {}
        self.Xbb0FLocut = {}#70
        self.Xbb2FLocut = {}#70
        self.Xbb1FLocut = {}#70
        self.SRcut = {}
        self.VRcut = {}
        self.CRcut = {}
        self.combined_cuts = {}
        self.CR_boundary= CR_boundary
        self.cuts_definition()


    def cuts_definition(self):
        for sample in self.all_sample_dfs:
            df = self.all_sample_dfs[sample]
            self.precut[sample] = (df['boostedPrecut'] == 1)
            self.largeRcut[sample] = (df['pt_h1Boosted'] > 450) & (df['pt_h2Boosted'] > 250) & (df['m_h1Boosted'] > 50) & (df['m_h2Boosted'] > 50)
            self.vbfcut[sample] = (abs(df['eta_VBFj1Boosted'] - df['eta_VBFj2Boosted']) > 3) & (df['pt_VBFj1Boosted'] > 20) & (df['pt_VBFj2Boosted'] > 20) & (df['m_VBFjjBoosted'] > 1000)
            self.completevbfcut[sample] = (abs(df['eta_VBFj1Boosted'] - df['eta_VBFj2Boosted']) > 3) & (df['pt_VBFj1Boosted'] > 20) & (df['pt_VBFj2Boosted'] > 20) & (df['m_VBFjjBoosted'] > 1000)
            self.Xbb0Fcut[sample] = (df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] <= 60)
            self.Xbb2Fcut[sample] = (df['HbbWP_h1Boosted'] > 60) & (df['HbbWP_h2Boosted'] > 60) # both failed
            self.Xbb1Fcut[sample] = ((df['HbbWP_h1Boosted'] > 60) & (df['HbbWP_h2Boosted'] <= 60)) | ((df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] > 60)) # either failed
            self.Xbb0FHPcut[sample] = (df['HbbWP_h1Boosted'] <= 50) & (df['HbbWP_h2Boosted'] <= 50)
            self.Xbb0FMPcut[sample] = (~((df['HbbWP_h1Boosted'] <= 50) & (df['HbbWP_h2Boosted'] <= 50))) & (df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] <= 60)
            self.Xbb0FTicut[sample] = (df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] <= 60)
            #self.Xbb0FLocut[sample] = (~((df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] <= 60))) & (df['HbbWP_h1Boosted'] <= 70) & (df['HbbWP_h2Boosted'] <= 70)
            self.Xbb0FLocut[sample] = (df['HbbWP_h1Boosted'] <= 70) & (df['HbbWP_h2Boosted'] <= 70)
            self.Xbb2FLocut[sample] = (df['HbbWP_h1Boosted'] > 70) & (df['HbbWP_h2Boosted'] > 70) # both failed
            self.Xbb1FLocut[sample] = ((df['HbbWP_h1Boosted'] > 70) & (df['HbbWP_h2Boosted'] <= 70)) | ((df['HbbWP_h1Boosted'] <= 70) & (df['HbbWP_h2Boosted'] > 70)) # either failed

            radius, fm1, fm2 = 1.6, 1500, 1900
            df.loc[:, 'Xhh'] = np.sqrt( ((df['m_h1Boosted'] - 124) / (fm1 / df['m_h1Boosted'] )) ** 2 + ((df['m_h2Boosted'] - 117) / (fm2 / df['m_h2Boosted'])) ** 2 )
            self.SRcut[sample] = df.loc[:, 'Xhh'] < radius
            #self.CRcut[sample] = (np.sqrt( (df['m_h1Boosted'] - 300) ** 2 + (df['m_h2Boosted'] - 250) ** 2 ) < 270) & ~self.SRcut[sample]
            #CRfunc = np.sqrt( ((df['m_h1Boosted'] - 124) / (0.1*df['m_h1Boosted'])) ** 2 + ((df['m_h2Boosted'] - 117) / (0.1*df['m_h2Boosted'])) ** 2 ) + np.sqrt( (df['m_h1Boosted'] - 120) ** 2 + (df['m_h2Boosted'] - 110) ** 2 )
            #VRbound, CRbound = 40, 60
            #CRfunc = np.sqrt( ((df['m_h1Boosted'] - 124) / (0.1*np.log(df['m_h1Boosted']))) ** 2 + ((df['m_h2Boosted'] - 117) / (0.1*np.log(df['m_h2Boosted']))) ** 2 ) + np.sqrt( (df['m_h1Boosted'] - 120) ** 2 + (df['m_h2Boosted'] - 110) ** 2 )
            CRfunc = np.sqrt( ((df['m_h1Boosted'] - 124) / (0.1*np.log(df['m_h1Boosted']))) ** 2 + ((df['m_h2Boosted'] - 117) / (0.1*np.log(df['m_h2Boosted']))) ** 2 )
            df.loc[:, 'CRfunc'] = CRfunc
            if self.rw_strategy != 5:
                VRbound, CRbound = 100, 170
                self.VRcut[sample] = (CRfunc < VRbound) & ~self.SRcut[sample]
                self.CRcut[sample] = (CRfunc >= VRbound) & (CRfunc < CRbound)
            else:
                VRbound, CRbound = 100, self.CR_boundary
                self.VRcut[sample] = (CRfunc < VRbound) & ~self.SRcut[sample]
                self.CRcut[sample] = (CRfunc >= VRbound) & (( df['m_h1Boosted'] + df['m_h2Boosted']) > CRbound)
                print(f'self.rw_strategy: {self.rw_strategy} ,self.CR_boundary:{self.CR_boundary}')

            self.combined_cuts[sample] = {
                'precut': self.precut[sample],
                'SR0F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0Fcut[sample] & self.SRcut[sample],
                'SR1F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1Fcut[sample] & self.SRcut[sample],
                'SR2F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2Fcut[sample] & self.SRcut[sample],
                'SR0FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0Fcut[sample] & self.SRcut[sample],
                'SR1FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb1Fcut[sample] & self.SRcut[sample],
                'SR2FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb2Fcut[sample] & self.SRcut[sample],
                'SR0FHP':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FHPcut[sample] & self.SRcut[sample],
                'SR0FMP':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FMPcut[sample] & self.SRcut[sample],
                'SR0FTi':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FTicut[sample] & self.SRcut[sample],
                'SR0FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FLocut[sample] & self.SRcut[sample],
                'SR1FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1FLocut[sample] & self.SRcut[sample],
                'SR2FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2FLocut[sample] & self.SRcut[sample],
                'SR0FHPnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FHPcut[sample] & self.SRcut[sample],
                'SR0FMPnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FMPcut[sample] & self.SRcut[sample],
                'SR0FTinoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FTicut[sample] & self.SRcut[sample],
                'SR0FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FLocut[sample] & self.SRcut[sample],
                'SR1FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb1FLocut[sample] & self.SRcut[sample],
                'SR2FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb2FLocut[sample] & self.SRcut[sample],
                '!SR0F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0Fcut[sample] & ~self.SRcut[sample],
                '!SR1F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1Fcut[sample] & ~self.SRcut[sample],
                '!SR2F':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2Fcut[sample] & ~self.SRcut[sample],
                '!SR0FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0Fcut[sample] & ~self.SRcut[sample],
                '!SR1FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb1Fcut[sample] & ~self.SRcut[sample],
                '!SR2FnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb2Fcut[sample] & ~self.SRcut[sample],
                '!SR0FHP':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FHPcut[sample] & ~self.SRcut[sample],
                '!SR0FMP':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FMPcut[sample] & ~self.SRcut[sample],
                '!SR0FTi':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FTicut[sample] & ~self.SRcut[sample],
                '!SR0FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FLocut[sample] & ~self.SRcut[sample],
                '!SR1FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1FLocut[sample] & ~self.SRcut[sample],
                '!SR2FLo':         self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2FLocut[sample] & ~self.SRcut[sample],
                '!SR0FHPnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FHPcut[sample] & ~self.SRcut[sample],
                '!SR0FMPnoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FMPcut[sample] & ~self.SRcut[sample],
                '!SR0FTinoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FTicut[sample] & ~self.SRcut[sample],
                '!SR0FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb0FLocut[sample] & ~self.SRcut[sample],
                '!SR1FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb1FLocut[sample] & ~self.SRcut[sample],
                '!SR2FLonoVBFcut': self.precut[sample] & self.largeRcut[sample] & self.Xbb2FLocut[sample] & ~self.SRcut[sample],
                'VR0F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0Fcut[sample] & self.VRcut[sample],
                'VR1F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1Fcut[sample] & self.VRcut[sample],
                'VR2F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2Fcut[sample] & self.VRcut[sample],
                'VR0FHP':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FHPcut[sample] & self.VRcut[sample],
                'VR0FMP':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FMPcut[sample] & self.VRcut[sample],
                'VR0FTi':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FTicut[sample] & self.VRcut[sample],
                'VR0FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FLocut[sample] & self.VRcut[sample],
                'VR1FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1FLocut[sample] & self.VRcut[sample],
                'VR2FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2FLocut[sample] & self.VRcut[sample],
                'VR0FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0Fcut[sample] & self.VRcut[sample],
                'VR1FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb1Fcut[sample] & self.VRcut[sample],
                'VR2FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb2Fcut[sample] & self.VRcut[sample],
                'VR0FHPnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FHPcut[sample] & self.VRcut[sample],
                'VR0FMPnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FMPcut[sample] & self.VRcut[sample],
                'VR0FTinoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FTicut[sample] & self.VRcut[sample],
                'VR0FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FLocut[sample] & self.VRcut[sample],
                'VR1FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb1FLocut[sample] & self.VRcut[sample],
                'VR2FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb2FLocut[sample] & self.VRcut[sample],
                'CR0F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0Fcut[sample] & self.CRcut[sample],
                'CR1F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1Fcut[sample] & self.CRcut[sample],
                'CR2F':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2Fcut[sample] & self.CRcut[sample],
                'CR0FHP':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FHPcut[sample] & self.CRcut[sample],
                'CR0FMP':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FMPcut[sample] & self.CRcut[sample],
                'CR0FTi':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FTicut[sample] & self.CRcut[sample],
                'CR0FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb0FLocut[sample] & self.CRcut[sample],
                'CR1FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb1FLocut[sample] & self.CRcut[sample],
                'CR2FLo':   self.precut[sample] & self.largeRcut[sample] & self.vbfcut[sample] & self.Xbb2FLocut[sample] & self.CRcut[sample],
                'CR0FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0Fcut[sample] & self.CRcut[sample],
                'CR2FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb2Fcut[sample] & self.CRcut[sample],
                'CR1FnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb1Fcut[sample] & self.CRcut[sample],
                'CR0FHPnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FHPcut[sample] & self.CRcut[sample],
                'CR0FMPnoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FMPcut[sample] & self.CRcut[sample],
                'CR0FTinoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FTicut[sample] & self.CRcut[sample],
                'CR0FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb0FLocut[sample] & self.CRcut[sample],
                'CR1FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb1FLocut[sample] & self.CRcut[sample],
                'CR2FLonoVBFcut':   self.precut[sample] & self.largeRcut[sample] & self.Xbb2FLocut[sample] & self.CRcut[sample],
####
                'CR0FLocompletevbf':   self.precut[sample] & self.largeRcut[sample] & self.completevbfcut[sample] & self.Xbb0FLocut[sample] & self.CRcut[sample],
                'CR1FLocompletevbf':   self.precut[sample] & self.largeRcut[sample] & self.completevbfcut[sample] & self.Xbb1FLocut[sample] & self.CRcut[sample],

            }

    def get_yields(self, df, description='', norm=None):
        weighted = df['totalWeightBoosted'].sum()
        if norm is None:
            norm = weighted
        count = df.shape[0]
        if weighted / norm > 0.1:
            print('{:25s} | {:8s} | {:10s} | {:.3f} ({:.1f}%)'.format(description, str(count), 'weighted', weighted, weighted / norm * 100))
        else:
            print('{:25s} | {:8s} | {:10s} | {:.3f} ({:.2f}%)'.format(description, str(count), 'weighted', weighted, weighted / norm * 100))
        return weighted, np.sqrt(count) / count

    @staticmethod
    def Zvalue(sig, bkg, b_err=None):
        ''' http://www.pp.rhul.ac.uk/~cowan/stat/medsig/medsigNote.pdf Eq(10) Eq(20)'''
        ntot = sig+bkg
        #significance_simple = sig / np.sqrt(bkg)
        significance = np.sqrt(2*((ntot*np.log(ntot/bkg)) - sig))
        #significance_with_error = np.sqrt(2*((ntot)*np.log(((ntot)*(bkg+b_err**2))/(bkg**2+(ntot)*b_err**2))-(bkg**2)/(b_err**2)*np.log(1+(b_err**2*sig)/(bkg*(bkg+b_err**2)))))
        return significance

    
    def _cuts_definition_tmp(self, df, key, fm1, fm2, radius):
        precut = (df['boostedPrecut'] == 1)
        largeRcut = (df['pt_h1Boosted'] > 450) & (df['pt_h2Boosted'] > 250) & (df['m_h1Boosted'] > 50) & (df['m_h2Boosted'] > 50)
        vbfcut = (abs(df['eta_VBFj1Boosted'] - df['eta_VBFj2Boosted']) > 3) & (df['pt_VBFj1Boosted'] > 20) & (df['pt_VBFj2Boosted'] > 20) & (df['m_VBFjjBoosted'] > 1000)
        Xbb0Fcut = (df['HbbWP_h1Boosted'] <= 60) & (df['HbbWP_h2Boosted'] <= 60)
        if key == 'SR':
            self.tmp_sigregioncut = np.sqrt( ((df['m_h1Boosted'] - 124) / (fm1 / df['m_h1Boosted'] )) ** 2 + ((df['m_h2Boosted'] - 117) / (fm2 / df['m_h2Boosted'])) ** 2 ) < radius
        elif key == 'SRbaseline':
            self.tmp_sigregioncut = np.sqrt( ((df['m_h1Boosted'] - 124) / (0.1*df['m_h1Boosted'])) ** 2 + ((df['m_h2Boosted'] - 117) / (0.1*df['m_h2Boosted'])) ** 2 ) < 1.6
        else:
            assert(0)
        self.tmp_full_cut = precut & largeRcut & Xbb0Fcut & vbfcut & self.tmp_sigregioncut

    def significance(self, signals, backgrounds, key, radius, fm1, fm2):
        df_sig = [self.all_sample_dfs[s] for s in signals]
        df_bkg = [self.all_sample_dfs[b] for b in backgrounds]
        df_sig, df_bkg = pd.concat(df_sig), pd.concat(df_bkg)

        self._cuts_definition_tmp(df_sig, key, fm1, fm2, radius)
        sig_yields, _ = self.get_yields(df_sig[self.tmp_full_cut], description=f'sig {key} ({pretty_value(radius)}, {fm1}, {fm2})')

        self._cuts_definition_tmp(df_bkg, key, fm1, fm2, radius)
        bkg_yields, bkg_error = self.get_yields(df_bkg[self.tmp_full_cut], description=f'bkg {key} ({pretty_value(radius)}, {fm1}, {fm2})')
        ZA = self.Zvalue(sig_yields, bkg_yields, b_err=bkg_yields*bkg_error)
        print('-'*4, ZA)

        return ZA


    def print_cutflow(self, samples=None):
        if samples is None:
            samples = self.all_sample_dfs
        for sample in samples:
            print(sample)
            self._print_cutflow(sample)
            print('-'*90)
    def _print_cutflow(self, sample):
        if self.rw_strategy != 5:
            df = self.all_sample_dfs[sample]
            precut = self.combined_cuts[sample]['precut']
            largeRcut = self.largeRcut[sample]
            Xbb0Fcut = self.Xbb0Fcut[sample]
            Xbb1Fcut = self.Xbb1Fcut[sample]
            vbfcut = self.vbfcut[sample]
            SRcut = self.SRcut[sample]
            CRcut =self.CRcut[sample]
            VRcut =self.VRcut[sample]
            tot_weighted, _ = self.get_yields(df[precut], 'Precut')
            self.get_yields(df[precut & largeRcut], 'LargeR jet cut', tot_weighted)
            print('-'*90)
            print('0F')
            self.get_yields(df[precut & largeRcut & Xbb0Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            for test in ['SR']:
                if test in self.combined_cuts[sample]:
                    self.get_yields(df[self.combined_cuts[sample][test]], f'- {test}', tot_weighted)
                    self.get_yields(df[self.combined_cuts[sample]['!'+test]], f'- Non-{test}', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
        elif ((self.rw_strategy == 5) & (self.alter == False)) :
            df = self.all_sample_dfs[sample]
            precut = self.combined_cuts[sample]['precut']
            largeRcut = self.largeRcut[sample]
            Xbb0Fcut = self.Xbb0FLocut[sample]
            Xbb1Fcut = self.Xbb1FLocut[sample]
            Xbb2Fcut = self.Xbb2FLocut[sample]
            vbfcut = self.vbfcut[sample]
            SRcut = self.SRcut[sample]
            CRcut =self.CRcut[sample]
            VRcut =self.VRcut[sample]
            print('0F')
            tot_weighted, _ = self.get_yields(df[precut], 'Precut')
            self.get_yields(df[precut & largeRcut], 'LargeR jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            for test in ['SR']:
                if test in self.combined_cuts[sample]:
                    self.get_yields(df[self.combined_cuts[sample][test]], f'- {test}', tot_weighted)
                    self.get_yields(df[self.combined_cuts[sample]['!'+test]], f'- Non-{test}', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)
            print('1F')
            self.get_yields(df[precut & largeRcut & Xbb1Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)
            print('2F')
            self.get_yields(df[precut & largeRcut & Xbb2Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)
        elif ((self.rw_strategy == 5) & (self.alter != False)) :
            df = self.all_sample_dfs[sample]
            precut = self.combined_cuts[sample]['precut']
            largeRcut = self.largeRcut[sample]
            Xbb0Fcut = self.Xbb0Fcut[sample]
            Xbb1Fcut = self.Xbb1Fcut[sample]
            Xbb2Fcut = self.Xbb2Fcut[sample]
            vbfcut = self.vbfcut[sample]
            SRcut = self.SRcut[sample]
            CRcut =self.CRcut[sample]
            VRcut =self.VRcut[sample]
            print('0F')
            tot_weighted, _ = self.get_yields(df[precut], 'Precut')
            self.get_yields(df[precut & largeRcut], 'LargeR jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            for test in ['SR']:
                if test in self.combined_cuts[sample]:
                    self.get_yields(df[self.combined_cuts[sample][test]], f'- {test}', tot_weighted)
                    self.get_yields(df[self.combined_cuts[sample]['!'+test]], f'- Non-{test}', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb0Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)
            print('1F')
            self.get_yields(df[precut & largeRcut & Xbb1Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb1Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)
            print('2F')
            self.get_yields(df[precut & largeRcut & Xbb2Fcut], 'Higgs tag cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut], 'VBF jet cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & SRcut], 'SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & ~SRcut], 'Non-SR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & VRcut], 'VR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & vbfcut & CRcut], 'CR cut', tot_weighted)
            self.get_yields(df[precut & largeRcut & Xbb2Fcut & ~SRcut], 'Non-SR noVBFcut cut', tot_weighted)
            print('-'*90)

    def df_apply_cut(self, whichcut):
        reduced_df = {}
        for sample in self.all_sample_dfs.keys():
            if sample in ['cache']:
                continue
            if sample != 'data': # data is blinded
                reduced_df['SR0F'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR0F'+whichcut]]
            reduced_df['NonSR0F'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR0F'+whichcut]]
            reduced_df['VR0F'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR0F'+whichcut]]
            reduced_df['CR0F'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR0F'+whichcut]]
            reduced_df['SR0FHP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR0FHP'+whichcut]]
            reduced_df['SR0FMP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR0FMP'+whichcut]]
            reduced_df['SR0FTi'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR0FTi'+whichcut]]
            #reduced_df['NonSR0FTi'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR0FTi'+whichcut]]
            reduced_df['VR0FHP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR0FHP'+whichcut]]
            reduced_df['VR0FMP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR0FMP'+whichcut]]
            reduced_df['VR0FTi'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR0FTi'+whichcut]]
            reduced_df['CR0FHP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR0FHP'+whichcut]]
            reduced_df['CR0FMP'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR0FMP'+whichcut]]
            reduced_df['CR0FTi'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR0FTi'+whichcut]]
            reduced_df['SR0FLo'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR0FLo'+whichcut]]
            reduced_df['NonSR0FLo'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR0FLo'+whichcut]]
            reduced_df['VR0FLo'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR0FLo'+whichcut]]
            reduced_df['CR0FLo'+sample] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR0FLo'+whichcut]]

            if sample in ['data','kvv0']:
                reduced_df[f'SR1F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR1F'+whichcut]]
                reduced_df[f'NonSR1F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR1F'+whichcut]]
                reduced_df[f'VR1F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR1F'+whichcut]]
                reduced_df[f'CR1F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR1F'+whichcut]]
                reduced_df[f'SR2F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR2F'+whichcut]]
                reduced_df[f'NonSR2F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR2F'+whichcut]]
                reduced_df[f'VR2F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR2F'+whichcut]]
                reduced_df[f'CR2F{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR2F'+whichcut]]
    
                reduced_df[f'SR1FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR1FLo'+whichcut]]
                reduced_df[f'NonSR1FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['!SR1FLo'+whichcut]]
                reduced_df[f'VR1FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR1FLo'+whichcut]]
                reduced_df[f'CR1FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR1FLo'+whichcut]]
                reduced_df[f'SR2FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['SR2FLo'+whichcut]]
                reduced_df[f'VR2FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['VR2FLo'+whichcut]]
                reduced_df[f'CR2FLo{sample}'] = self.all_sample_dfs[sample][self.combined_cuts[sample]['CR2FLo'+whichcut]]

        self.SR_df[whichcut] = reduced_df
        return reduced_df
